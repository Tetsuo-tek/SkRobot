#ifndef ABSTRACTWEBWIDGET_H
#define ABSTRACTWEBWIDGET_H

#include "Core/Object/skobject.h"
#include "Core/System/Time/skelapsedtime.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum WebWidgetType
{
    WT_NOTYPE,
    WT_WIDGET,      //IT IS A LEAF
    WT_FORM,        //IT IS A LEAF
    WT_CONTAINER    //IT HAS CHILDREN; IT IS NOT A LEAF
};

class SkTemplateManager;
class SkHtmlTemplate;
class WebWidgetContainer;

struct WebWidgetTemplate
{
    SkString filePath;
    SkArgsMap customize;
};

class AbstractWebWidget extends SkObject
{
    public:
        void setLabel(CStr *label);

        void setHtmlTemplate(CStr *htmlTemplate, SkArgsMap &customize);
        void setInitJsTemplate(CStr *jsTemplatePath, SkArgsMap &customize);
        void setTickJsTemplate(CStr *jsTemplatePath, SkArgsMap &customize, double intervalSeconds);

        bool hasHtmlTemplate();
        WebWidgetTemplate &htmlTemplate();
        SkHtmlTemplate *buildHtml(SkTemplateManager *manager);

        bool hasInitJsTemplate();
        WebWidgetTemplate &initJsTemplate();
        bool buildInitJs(SkTemplateManager *manager, SkString &outputCode);

        double tickTimeInterval();
        bool hasTickJsTemplate();
        WebWidgetTemplate &tickJsTemplate();
        bool buildTickJs(SkTemplateManager *manager, SkString &outputCode);

        CStr *label();
        CStr *id();
        WebWidgetContainer *container();
        WebWidgetType widgetType();

    protected:
        AbstractConstructor(AbstractWebWidget, SkObject);

        WebWidgetType t;

        SkString lbl;
        SkString wid;

        WebWidgetTemplate html;
        WebWidgetTemplate initJs;

        SkElapsedTime tickChrono;
        double interval;
        WebWidgetTemplate tickJs;
};

#endif // ABSTRACTWEBWIDGET_H
