# External-satellite examples

It is required an instance of SkRobot running.

Before to try examples, you need to define an evironment variable named `ROBOT_ADDRESS` related to SkRobot target connection.

If you want to reach SkRobot via local socket:

```sh
export ROBOT_ADDRESS=local:/tmp/<RobotName>
```

Otherwise, if you want to reach SkRobot via tcp socket:

```sh
export ROBOT_ADDRESS=tcp:<RobotTcpAddress>
```

or, when the Robot port is not the default for the FlowProtocol (9000):

```sh
export ROBOT_ADDRESS=tcp:<RobotTcpAddress>:<RobotTcpPort>
```

Set `ROBOT_ADDRESS` on .bashrc or .profile user file, if you don't want rewrite every time.

## Sk/PySketch
Python sketches require to be in the same directory of the PySketch engine, so you can link the engine.

Using PySketch from [`pck`](https://gitlab.com/Tetsuo-tek/pck) base system:

```sh
cd SkRobot/examples
ln -s /opt/Sk/src/PySketch
```

or, without `pck`:

```sh
git clone https://gitlab.com/Tetsuo-tek/PySketch.git
cd SkRobot/examples
ln -s /path/to/PySketch
```

After, you can launch the sketch:

```sh
./<sketchName>.py
```

### Examples
* **`publisher.py` / `subscriber.py`**: The same above, but using sketches for Sk/PySketch in Python.

* **`camera-capture.py` / `camera-display.py`**: The first captures a MJPEG stream from camera and delivers it through SkRobot; data coming to the second satellite that display the flow on a simple window.

## Sk/C++

Examples need to be compiled through [SkMake](https://gitlab.com/Tetsuo-tek/SkMake), requiring [SpecialK](https://gitlab.com/Tetsuo-tek/SpecialK) framework dependency.

if `pck` is installed on your system, you have already a copy of `skmake` binary and of SpecialK framework. Otherwise, you must clone and install them manually, reading instruction specified on their README.

Using [`pck`](https://gitlab.com/Tetsuo-tek/pck):

```sh
cd SkRobot/examples
ln -s /opt/Sk/src/SpecialK
```

or, without `pck`:

```sh
git clone https://gitlab.com/Tetsuo-tek/SpecialK.git
cd SkRobot/examples
ln -s /path/to/SpecialK
```

Once you have an usable copy of `skmake` binary, and source directory for SpecialK, you can compile examples using the default `skmake.json` stored inside each directory:

```sh
skmake -m skmake.json -c <num_cores>
```

The binary will be produced inside the directory.

### Examples

* **`Publisher-FlowASync` / `Subscriber-FlowASync`**: Publisher and Subscriber, emulating Talker/Listener example from ROS, but using SpecialK and SkRobot; SkFlowAsynch is subclassed directly making satellites upon the asynchronous connection.

* **`AudioCapture` / `AudioPlayback`**: the first capture PCM audio sending buffers on FlowNetwork, while the second can play these PCM published buffers; they subsclass `SkFlowSat` making more simple to manage the satellites and their activities about FlowNetwork capabilities. Examples show also command-line interface management.
