#ifndef ROBOTDB_H
#define ROBOTDB_H

#include <Core/System/skosenv.h>
#include "Core/Containers/skvariant.h"
#include "Core/Containers/sktreemap.h"

#include "Core/System/Network/TCP/skflattcpsocket.h"

class RobotDB extends SkFlatObject
{
    public:
        RobotDB();
        int exec();

    private:
        SkVariant db;
        SkTreeMap<SkString, SkVariant> dbMap;
        SkString dbFilePath;
        //SkFlatSignal sig;

        void load();
        void buildCLI(SkStringList &acceptedKeys);

        //static void cb(SkFlatObject *_this, SkFlatObject *referer);
        //AllowThis(cb);
};

#endif // ROBOTDB_H
