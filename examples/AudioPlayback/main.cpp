#include "audioplay.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();
    cli->add("--device",     "-d",  "default",           "Setup the playback device");
    cli->add("--src-chan",   "-s",  "guest.PCM.Short",   "Setup the PCM channel to play");

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new AudioPlay;
    return skApp->exec();
}
