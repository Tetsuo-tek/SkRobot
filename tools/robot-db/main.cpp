#include "robotdb.h"

int main(int argc, char *argv[])
{
    osEnvironment->init(argc, argv);
    return (new RobotDB)->exec();
}
