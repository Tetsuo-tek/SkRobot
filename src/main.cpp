#include "physicalbody.h"
#include "Core/System/skosenv.h"

SkApp *skApp = nullptr;

#include "Core/System/IPC/skprocessdeperecated.h"

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    logger->enablePID(false);
    logger->enableDateTime(false);
    logger->enableEscapes(true);
    logger->enablePrettyName(false);
    logger->enableSrcFileName(false);
    logger->enableDebug(false);
    logger->enablePlusDebug(false);

    SkCli *cli = skApp->appCli();

    cli->add("--set-working-dir",   "-d", "",   "Setup the working directory for SkRobot");
    cli->add("--set-mods-conf",     "-m", "",   "Setup the json filepath containing modules to start");

    if (!cli->check())
        exit(1);

    skApp->init(10000, 250000, SK_TIMEDLOOP_RT);
    new PhysicalBody;

    return skApp->exec();
}
