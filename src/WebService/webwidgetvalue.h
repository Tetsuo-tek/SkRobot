#ifndef WEBWIDGETVALUE_H
#define WEBWIDGETVALUE_H

#include "abstractwebwidget.h"
#include "Core/System/Network/FlowNetwork/skflowprotocol.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

enum WebWidgetSourceType
{
    WST_NOTYPE,
    WST_VALUEWIDGET,        //ITS DATA GETS UPDATED FROM FlowServer DB
    //    WST_FLOW,        //ITS DATA GETS UPDATED FROM FlowServer chanID
    WST_TARGETWIDGET       //IT WILL BE TRIGGERED ON SOURCE WIDGET run
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class WebWidgetValue extends AbstractWebWidget
{
    public:
        Constructor(WebWidgetValue, AbstractWebWidget);

        void setDataSourceType(WebWidgetSourceType type);
        void setID(CStr *widgetID);

        void setPair(CStr *pairDbName, CStr *pairKey);

        void setMin(const SkVariant &minimum);
        void setMax(const SkVariant &maximum);
        void setUdm(CStr *udm);

        void addTarget(WebWidgetValue *w);

        void setRunJsTemplate(CStr *codeTemplatePath, SkArgsMap &customize);
        bool hasRunJsTemplate();
        WebWidgetTemplate &runJsTemplate();

        //CODE WILL BE BUILT ALSO IF THIS W HAS NOT A runJs AND IT HA TARGET WITH runJs
        //RETURN false ONLY IF THIS W OR TARGET HAVE runJs BUT THERE ARE ERRORs
        bool buildRunJs(SkTemplateManager *manager, SkString &outputCode, SkString &val);

        WebWidgetSourceType widgetSourceType();

        CStr *pairsDatabaseName();
        CStr *pairKey();

        bool isLimited();
        SkVariant &getMin();
        SkVariant &getMax();

        CStr *udm();

        bool hasTargets();
        SkList<WebWidgetValue *> &targets();

    private:
        WebWidgetSourceType source_t;
        SkString dbName;
        SkString k;
        SkVariant min;
        SkVariant max;
        SkString u;
        SkList<WebWidgetValue *> targetWidgets;

        WebWidgetTemplate runJs;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // WEBWIDGETVALUE_H
