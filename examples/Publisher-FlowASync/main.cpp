#include "publisher.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    //VERY SLOW, to compare with ROS talker example
    skApp->init(500000, 750000, SK_TIMEDLOOP_RT);

    Publisher *a = new Publisher;
    Attach(skApp->started_SIG, pulse, a, init, SkOneShotQueued);

    return skApp->exec();
}
