#ifndef PHYSICALBODY_H
#define PHYSICALBODY_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "robotabstractapplication.h"
#include <Modules/skmodulesmanager.h>

#include "config.h"
#include "Init/init.h"
#include "authenticator.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define ROBOT_MAJORVERSION              1
#define ROBOT_MINORVERSION              0
#define ROBOT_CODENAME                  "BlackMonkey"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class PhysicalBody extends RobotAbstractApplication
{
    public:
        Constructor(PhysicalBody, RobotAbstractApplication);

        Slot(onReady);

        Slot(changeConfig);
        Slot(testCmd);
        Slot(onWrkEvaluatedCmd);

        Config *cfg();
        Init *serviceInit();
        Authenticator *authenticator();

        void updateCommand(SkWorkerCommand *cmd)                override;

    private:
        Config config;
        Init *init;
        Authenticator *authManager;

        SkElapsedTime checksChrono;
        double checkSecs;

#if defined(ENABLE_STATGRAB)
        SkElapsedTime systemServiceChrono;
#endif

        bool onAppInit()                                        override;
        void onAppExit()                                        override;

        void onFastTick()                                       override;
        void onSlowTick()                                       override;
        void onOneSecTick()                                     override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // PHYSICALBODY_H
