#ifndef WEBWIDGETPARSER_H
#define WEBWIDGETPARSER_H

#include "webwidgetpairstable.h"
#include "webwidgetform.h"

#include <Core/System/Filesystem/skfsutils.h>
#include <Core/Containers/sktreemap.h>
#include <Core/Scripting/SkCodeTemplate/sktemplatemodeler.h>

class SkFlowSync;
class WebPageWidgets;

class WebWidgetParser extends SkTemplateModeler
{
    public:
        WebWidgetParser();

        bool parse(CStr *filePath, SkFlowSync *flow);

    protected:
        SkFlowSync *sync;
        SkPathInfo wuiPathInfo;

        virtual void addWidgetContainer(WebWidgetContainer *)   = 0;
        virtual void addWidget(WebWidgetValue *)                     = 0;
        virtual void addWidgetForm(WebWidgetForm *)             = 0;
        virtual void onFileEvaluation(SkFileInfo *)             = 0;

    private:
        SkArgsMap widgets;

        bool parseAbstractWidgetsList(SkVariant &v, WebWidgetContainer *container);
        bool parseWidgetContainer(SkArgsMap &m, WebWidgetContainer *container);
        bool parseWidget(SkArgsMap &m, WebWidgetContainer *container);
        bool parseWidgetPairs(SkArgsMap &m, WebWidgetContainer *container);
        bool parseWidgetForm(SkArgsMap &m, WebWidgetContainer *container);

        bool evaluateFile(CStr *filePath, SkString &output);

        bool onKeyNotFound(CStr *k, SkString &replaced) override;
};

#endif // WEBWIDGETPARSER_H
