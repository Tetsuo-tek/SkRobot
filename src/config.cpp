#include "config.h"
#include "physicalbody.h"
#include <Core/App/skapp.h>
#include <Core/System/skosenv.h>
#include <Core/System/Filesystem/skfsutils.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

Config::Config()
{
    setObjectName("Config");
}

bool Config::init(PhysicalBody *physicalBody)
{
    owner = physicalBody;

    set("Setup", "Sets Robot configuration", owner->changeConfig_SLOT);

    SkWorkerParam *p = addParam("tickMode", "Tick mode", T_INT8, true, SK_TIMEDLOOP_SLEEPING, "Sets the Robot tick mode (0:rt, 1:sleeping, 2:sck-waiting)");
    p->addOption("Sleeping", SK_TIMEDLOOP_SLEEPING);
    p->addOption("Real-time", SK_TIMEDLOOP_RT);
    p->addOption("Waiting", SK_EVENTLOOP_WAITING);

    addParam("sharedAddress", "Shared network address", T_STRING, false, "", "Sets Robot shared networking address");

    addParam("fastTickTime", "Fast tick time", T_UINT32, true, 10000, "Sets Robot fast-tick time interval in microseconds");
    addParam("slowTickTime", "Slow tick time", T_UINT32, true, 250000, "Sets Robot slow-tick time interval in microseconds");
    addParam("checkTickTime", "Check tick time", T_UINT8, true, 10, "Sets Robot check-routine time interval in seconds");

    addParam("appName", "Name", T_STRING, true, "Robot", "Sets the name-identity of this robot");
    addParam("workingDir", "Working directory", T_STRING, true, "", "Sets the ABSOLUTE working directory for SkRobot");
    addParam("modsCfgFile", "Modules", T_STRING, true, "", "Sets the filepath containing modules to start");

    addParam("enableTcpFs", "Tcp fs://", T_BOOL, true, true, "Enables/Disables tcp Flow service");
    addParam("fsListenAddr", "Flow listenaddress", T_STRING, true, "0.0.0.0", "Sets Flow tcp listen address");
    addParam("fsListenPort", "Flow listen tcp-port", T_UINT16, true, 9000, "Sets Flow tcp listen tcp-port");
    addParam("fsMaxConnQueued", "Flow max queued", T_UINT16, true, UShort(10), "Sets max connection on Flow tcp service accepting queue ");
    addParam("fsNodeCfgFile", "FsNode setup", T_STRING, false, "", "Sets the node-behaviour setup");

#if defined(ENABLE_HTTP)
    addParam("httpDir", "Directory for static www", T_STRING, true, "www", "Sets the RELATIVE path for static www contents");
    addParam("httpListenAddr", "Http listen address", T_STRING, true, "0.0.0.0", "Sets Http listen address");
    addParam("httpListenPort", "Flow listen tcp-port", T_UINT16, true, 9001, "Sets Http listen tcp-port");
    addParam("httpMaxHeaderSize", "Max http header size", T_UINT64, true, ULong(100000), "Sets maximum bytes length accepted for http requests");
    addParam("httpMaxConnQueued", "Http max queued", T_UINT16, true, UShort(10), "Sets max connection on Http service accepting queue ");
    addParam("httpIdleTimeoutInterval", "IDLE timeout seconds", T_UINT16, true, UShort(20), "Sets max seconds interval to kill an idle http connection");
    addParam("httpSslEnabled", "Enables SSL", T_BOOL, true, false, "Enables/Disables the ssl protocol for Http service");
    addParam("httpSslCertFile", "Http SSL certificate", T_STRING, false, "ssl/selfsigned.crt", "Sets filepath for the ssl-certificate");
    addParam("httpSslKeyFile", "Http SSL key", T_STRING, false, "ssl/selfsigned.key", "Sets filepath for the ssl-key");
#endif

#if defined(ENABLE_REDIS)
    addParam("enableRedis", "Redis pairs mirror", T_BOOL, true, true, "Enables/Disables Redis pairs mirror and synch");
    addParam("redisAddr", "Redis address", T_STRING, true, "127.0.0.1", "Sets Redis address");
    addParam("redisPort", "Redis tcp-port", T_UINT16, true, UShort(6379), "Sets Redis tcp-port");
#endif

    if (skApp->appCli()->userAppCli().isEmpty())
        FlatMessage("No CLI parameters");

    else
        FlatMessage("CLI parameters: " << skApp->appCli()->userAppCli().join(" "));

    if (!checkCfgFile())
        return false;

    if (!checkCfgPair())
        return false;

    //save();
    return true;
}

bool Config::checkCfgFile()
{
    if (!SkFsUtils::exists("robot.json"))
        return true;

    SkVariant v;

    if (!SkFsUtils::readJSON("robot.json", v))
        return false;

    SkArgsMap cfg;
    v.copyToMap(cfg);

    SkCli *cli = skApp->appCli();

    if (cli->isUsed("--set-working-dir"))
        cfg["workingDir"] = cli->value("--set-working-dir");

    if (cli->isUsed("--set-mods-conf"))
        cfg["modsCfgFile"] = cli->value("--set-mods-conf");

    SkString cfgStr;
    v.toJson(cfgStr);

    SkString newHashCfg = ::stringHash(cfgStr);

    if (newHashCfg != hashCfg)
    {
        FlatWarning("Configuration file is CHANGED: robot.json");

        if (!setup(cfg))
            return false;
    }

    return true;
}

bool Config::checkCfgPair()
{
    SkFlowPairsDatabase *mainDB = owner->serviceInit()->flowServerMainDB();
    SkString configKey("Application_CFG");

    SkArgsMap cfg;

    if (!mainDB->existsVariable(configKey.c_str()))
    {
        FlatWarning("Configuration does NOT exist; creating a default-configuration for: " << configKey);

        SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = parameters().iterator();

        //IT IS VERY INEFFICIENT!

        while(itr->next())
        {
            SkWorkerParam *param = itr->item().value();
            cfg[param->key()] = param->defaultValue();
        }

        delete itr;

        if (!setup(cfg))
            return false;
    }

    else
    {
        SkVariant &cfgVal = mainDB->getVariable(configKey.c_str());
        cfgVal.copyToMap(cfg);

        if (setup(cfg))
            FlatMessage("Configuration exists and it is valid: " << configKey);

        else
        {
            FlatError("Configuration exists, but it is NOT valid: " << configKey);
            mainDB->delVariable(configKey.c_str());
            return false;
        }
    }

    return true;
}

bool Config::setup(SkArgsMap &cfg)
{
    SkString cfgStr;
    cfg.toString(cfgStr);

    SkString lastHash = hashCfg;
    hashCfg = ::stringHash(cfgStr);

    if (lastHash == hashCfg)
        return true;

    if (!load(cfg))
    {
        cfg.clear();
        FlatWarning("Loaded configuration the same configuration as the old [md5 -> " << hashCfg << "]: " << cfgStr);
        return false;
    }

    FlatMessage("Loaded configuration [md5 -> " << hashCfg << "]: " << cfgStr);
    save();

    if (!SkFsUtils::exists("robot.json"))
    {
        FlatWarning("Writing configuration file: robot.json");
        AssertKiller(!SkFsUtils::writeJSON("robot.json", cfg, true));
    }

    return true;
}

void Config::save()
{
    SkTreeMap<SkString, SkVariant> cfg;
    SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = parameters().iterator();

    //IT IS VERY INEFFICIENT!

    while(itr->next())
    {
        SkWorkerParam *param = itr->item().value();

        //cout << itr->item().key() << " " << param->defaultValue() << "\n";
        cfg[param->key()] = param->value();
    }

    delete itr;

    SkFlowPairsDatabase *mainDB = owner->serviceInit()->flowServerMainDB();
    mainDB->setVariable("Application_CFG", cfg);
    FlatMessage("Saved configuration (Application_CFG) on Main db-pair [md5 -> " << hashCfg << "]");
}

CStr *Config::hash()
{
    return hashCfg.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
