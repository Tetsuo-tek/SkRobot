#include "physicalbody.h"
#include "Core/System/skosenv.h"
#include "Core/System/Filesystem/skfsutils.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(PhysicalBody, RobotAbstractApplication)
{
    setObjectName("Robot-UNNAMED");
    StaticMessage("SkRobot version: " << ROBOT_MAJORVERSION << "." << ROBOT_MINORVERSION << " (" << ROBOT_CODENAME << ")");

    authManager = new Authenticator(this);
    authManager->setup("users.d/");
    checkSecs = 10.;

    SlotSet(onReady);

    SlotSet(testCmd);
    SlotSet(changeConfig);
    SlotSet(onWrkEvaluatedCmd);

    init = new Init(this);
    Attach(init, ready, this, onReady, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool PhysicalBody::onAppInit()
{
    ObjectMessage("Init ..");

    if (!init->start())
    {
        ObjectError("Application is NOT initialized");
        return false;
    }

    updateCommand(&config);
    addCommand(&config);
    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(PhysicalBody, onReady)
{
    SilentSlotArgsWarning();

    addCommand("TestCommand", "Test service command", testCmd_SLOT);
    init->flowServer()->addServiceChannel("ServiceTest");

    ULong fast = config.value("fastTickTime").toUInt64();
    ULong slow = config.value("slowTickTime").toUInt64();
    SkLoopTimerMode mode = static_cast<SkLoopTimerMode>(config.value("tickMode").toUInt8());

    eventLoop()->changeTimerMode(mode);
    eventLoop()->changeFastZone(fast);
    eventLoop()->changeSlowZone(slow);

    checkSecs = config.value("checkTickTime").toDouble();

    setEnabled(true);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void PhysicalBody::onAppExit()
{
    ObjectWarning("Expiring ..");
    init->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  App WorkerTransactions

SlotImpl(PhysicalBody, testCmd)
{
    SilentSlotArgsWarning();

    SkArgsMap cmdMap;
    SkVariant &v = Arg_AbstractVariadic;
    v.copyToMap(cmdMap);

    SkWorkerCommand *cmd = commands()["testCmd"];
    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    if (cmd->load(cmdMap))
    {
        updateCommand(cmd);

        if (t)
        {
            t->setResponse(cmdMap);
            t->goBack();
        }
    }

    else
    {
        if (t)
        {
            t->setError("Command NOT valid");
            t->setResponse(cmdMap);
            t->goBack();
        }
    }
}

SlotImpl(PhysicalBody, changeConfig)
{
    SilentSlotArgsWarning();

    SkVariant &v = Arg_AbstractVariadic;

    SkArgsMap cfg;
    v.copyToMap(cfg);

    /*SkString s;
    cmdMap.toString(s, true);
    cout << "!!!!!!!!!! " << s << "\n";*/

    SkArgsMap lastCfg;
    init->flowServerMainDB()->getVariable("Application_CFG").copyToMap(lastCfg);

    SkWorkerTransaction *t = DynCast(SkWorkerTransaction, referer);

    if (config.setup(cfg))
    {
        SkBinaryTreeVisit<SkString, SkWorkerParam *> *itr = config.parameters().iterator();

        while(itr->next())
        {
            SkString &k = itr->item().key();
            SkWorkerParam *p = itr->item().value();
            SkVariant &lastValue = lastCfg[k];

            if (p->value() != lastValue)
            {
                ObjectWarning("Configuration parameter has changed: "
                              << k << "[" << p->value() << " -> " << lastValue << "]");
            }
        }

        delete itr;

        config.save();
        updateCommand(&config);

        t->goBack();
    }

    else
    {
        t->setError("Configuration NOT valid");
        t->setResponse(cfg);
        t->goBack();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  App Ticks

void PhysicalBody::onFastTick()
{
    if (!isEnabled())
        return;
}

void PhysicalBody::onSlowTick()
{
    if (!isEnabled())
        return;

    if (checksChrono.stop() > checkSecs)
    {
        //
        checksChrono.start();
    }
}

void PhysicalBody::onOneSecTick()
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(PhysicalBody, onWrkEvaluatedCmd)
{
    SilentSlotArgsWarning();

    SkWorkerTransaction *t = dynamic_cast<SkWorkerTransaction *>(referer);

    SkString s;
    t->argument().toString(s);

    if (t->hasErrors())
        ObjectError("LOCAL-Commander transaction has returned back with ERRORs: " << t->worker()->objectName() << "::" << t->command() << " [args: " << s << "] -> error: " << t->error());

    else
    {
        SkString json;
        t->response().toString(json);
        ObjectMessage("LOCAL-Commander transaction has returned back: " << t->worker()->objectName() << "::" << t->command() << " [args: " << s << "] -> response: " << json);
    }

    SkString cmd = t->command();
    t->destroyLater();

    if (cmd == "changeConfig" )
        init->respawn();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// Save new command transaction with (new) values

void PhysicalBody::updateCommand(SkWorkerCommand *cmd)
{
    SkString k = cmd->command();
    k.append("_TRANS");

    SkArgsMap m;
    m["owner"] = objectName();
    m["ownerType"] = typeName();

    cmd->toMap(m);

    init->flowServerMainDB()->setVariable(k.c_str(), m);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

Config *PhysicalBody::cfg()
{
    return &config;
}

Init *PhysicalBody::serviceInit()
{
    return init;
}

Authenticator *PhysicalBody::authenticator()
{
    return authManager;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
