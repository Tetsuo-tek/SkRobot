#!/usr/bin/env pysketch-executor

###############################################################################
#SKETCH

from PySketch.flowsat       import FlowSat
from PySketch.loop          import LoopTimerMode
from PySketch.log           import msg, dbg, wrn, err, cri, printPair

userName = "User1"
userPasswd = "password"

sat = FlowSat()
service = None
serviceGlobalName = ""

def setup():
    global serviceGlobalName

    sat.setLogin(userName, userPasswd)
    
    if not sat.connect():
        return False
    
    sat.setTickTimer(0.010, 0.050, 0.200, LoopTimerMode.TIMEDLOOP_RT)

    sat.channelAdded.attach(onChannelAdded)
    sat.serviceRequest.attach(onServiceRequest)

    serviceName = "ServiceTestOnClient"

    if not sat.addServiceChannel("ServiceTestOnClient"):
        return False

    serviceGlobalName = "{}.ServiceTestOnClient".format(sat._userName)
    return True

def loop():
    sat.tick()
    return sat.isConnected()

###############################################################################
#CALLBACKs

def onChannelAdded(ch):
    global service

    if ch.name == serviceGlobalName:
        service = ch
        msg("ServiceChannel is READY: {}".format(service.name))

def onServiceRequest(chanID, hashStr, cmdName, val):
    # Sat could build more service-channels
    if service.chanID == chanID:
        #data echo test
        dbg("ServiceChannel request RECEIVED [cmdName: {}; hash: {}]: {}".format(cmdName, hashStr, val))
        
        val = {"status" : True, "data" : val}
        sat.sendServiceResponse(chanID, hashStr, val)
    
###############################################################################
