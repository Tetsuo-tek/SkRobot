#include "robotabstractapplication.h"

AbstractConstructorImpl(RobotAbstractApplication, SkAbstractWorkerObject)
{
    enabled = false;
    appWouldExit = false;

    SlotSet(appInit);
    SlotSet(appExitRequest);
    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);
    SignalSet(initialized);
    SlotSet(onGameClosed);

    Attach(skApp->started_SIG, pulse, this, appInit, SkOneShotQueued);
}

void RobotAbstractApplication::setEnabled(bool enable)
{
    if (enable)
    {
        if (enabled)
        {
            ObjectError("This application is ALREADY enabled");
            return;
        }

        enabled = true;

        Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkAttachMode::SkDirect);
        Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkAttachMode::SkQueued);
        Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkAttachMode::SkQueued);
    }

    else
    {
        if (!enabled)
        {
            ObjectError("This application is ALREADY disabled");
            return;
        }

        enabled = false;

        Detach(skApp->fastZone_SIG, pulse, this, fastTick);
        Detach(skApp->slowZone_SIG, pulse, this, slowTick);
        Detach(skApp->oneSecZone_SIG, pulse, this, oneSecTick);
    }
}

bool RobotAbstractApplication::isEnabled()
{
    return enabled;
}

SlotImpl(RobotAbstractApplication, appInit)
{
    SilentSlotArgsWarning();

    AssertKiller(!onAppInit());
    Attach(skApp->kernel_SIG, pulse, this, appExitRequest, SkOneShotQueued);

    initialized();
}

SlotImpl(RobotAbstractApplication, appExitRequest)
{
    SilentSlotArgsWarning();

    int32_t sig = skApp->kernel_SIG->pulse_SIGNAL.getParameters()[0].toInt32();

    if (sig != SIGINT
            && sig != SIGINT
            && sig != SIGTERM
            && sig != SIGQUIT
            && !appWouldExit)
    {
        return;
    }

    ObjectWarning("Forcing application shutdown ..");

    onAppExit();
}

SlotImpl(RobotAbstractApplication, onGameClosed)
{
    SilentSlotArgsWarning();

    skApp->quit();
}

SlotImpl(RobotAbstractApplication, fastTick)
{
    SilentSlotArgsWarning();

    onFastTick();
}

SlotImpl(RobotAbstractApplication, slowTick)
{
    SilentSlotArgsWarning();

    onSlowTick();
}

SlotImpl(RobotAbstractApplication, oneSecTick)
{
    SilentSlotArgsWarning();

    onOneSecTick();
}

void RobotAbstractApplication::quit()
{
    ObjectWarning("Request for application shutdown ..");
    appWouldExit = true;
    eventLoop()->invokeSlot(appExitRequest_SLOT, this, this);
}
