#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.flowproto         import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat           import FlowSat

###############################################################################

import cv2
import numpy                    as np
import mediapipe                as mp
import tensorflow               as tf
from tensorflow.keras.models    import load_model

###############################################################################
# GLOBAL

userName = "User1"
userPasswd = "password"

sat = FlowSat()

longChanName = "User1.Camera.MJPEG"
videoSrcChan = None

w = -1
h = -1
fps = -1
tickInterval = -1

###############################################################################
# SKETCH

def setup():
    print("[SETUP] ..")

    sat.setLogin(userName, userPasswd)

    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    sat.setGrabDataCallBack(onDataGrabbed)

    ok = sat.connect()

    if ok:
        sat.setSpeedMonitorEnabled(True)
        print("[LOOP] ..")

    return ok

def loop():
    sat.tick()
    return sat.isConnected()

###############################################################################
# CALLBACKs

def onChannelAdded(ch):
    global videoSrcChan
    global w
    global h
    global fps
    global tickInterval

    if ch.name == longChanName:
        print("Channel ADDED: {}".format(ch.name))
        videoSrcChan = ch

        sync = sat.newSyncClient()
        sync.setCurrentDbName(longChanName)
        res = sync.getVariable("resolution")
        res = res.split("x")
        w = int(res[0])
        h = int(res[1])
        fps = sync.getVariable("fps")
        tickInterval = sync.getVariable("tickTimePeriod")
        sync.close()

        sat.setTickTimer(tickInterval, tickInterval*20)
        sat.subscribeChannel(videoSrcChan.chanID)

def onChannelRemoved(ch):
    global videoSrcChan

    if ch.name == longChanName:
        print("Channel REMOVED: {}".format(ch.name))
        videoSrcChan = None
        cv.destroyAllWindows()

def onDataGrabbed(chanID, data):
    if videoSrcChan is None:
        return

    if chanID == videoSrcChan.chanID:
        jpg = np.frombuffer(data, np.uint8)
        f = cv.imdecode(jpg, cv.IMREAD_COLOR)
        f = cv.resize(f, (int(w/2),int(h/2)), interpolation=cv.INTER_NEAREST)

###############################################################################
