#include "webwidgetcontainer.h"
#include "Core/Scripting/SkCodeTemplate/sktemplatemanager.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(WebWidgetContainer, AbstractWebWidget)
{
    t = WebWidgetType::WT_CONTAINER;
}

void WebWidgetContainer::setID(CStr *id)
{
    wid = id;
    SkString n(wid);
    n.append("_WC");
    setObjectName(n.c_str());
}

void WebWidgetContainer::add(AbstractWebWidget *w)
{
    w->setParent(this);
    widgets << w;

    if (SkString::isEmpty(w->id()))
        ObjectMessage("ADDED child without id");

    else
        ObjectMessage("ADDED child: " << w->id());
}

void WebWidgetContainer::remove(AbstractWebWidget *w)
{
    widgets.remove(w);

    if (SkString::isEmpty(w->id()))
        ObjectMessage("REMOVED child without id");

    else
        ObjectMessage("REMOVED child: " << w->id());
}

SkList<AbstractWebWidget *> &WebWidgetContainer::childrenWidgets()
{
    return widgets;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
