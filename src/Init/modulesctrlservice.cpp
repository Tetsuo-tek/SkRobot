#include "modulesctrlservice.h"
#include "../physicalbody.h"

#include <Modules/skmodulesmanager.h>
#include <Core/System/Filesystem/skfsutils.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(ModulesCtrlService, AbstractService)
{
    quittedModules = 0;

    flowService = nullptr;
    modsManager = nullptr;

    SlotSet(onModsManagerStart);
    SlotSet(onModsManagerStop);
    SlotSet(onModThreadInstanced);
    SlotSet(onModTickAvgCheck);
    SlotSet(onModuleQuitted);

    SlotSet(onAppTickAvgChange);
    SlotSet(onAppTickAdjust);

    setObjectName("ModulesCtrlService");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void ModulesCtrlService::setup(FlowService *fs, CStr *superToken)
{
    flowService = fs;
    token = superToken;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool ModulesCtrlService::onInit()
{
    ObjectWarning("Starting ModulesManager ..");

    SkString modsCfgFile = cfg->value("modsCfgFile").toString();
    AssertKiller(!modsCfgFile.isEmpty() && !mods.fromFile(modsCfgFile.c_str()));

    if (mods.isEmpty())
    {
        mods["Dummy"] = "SkDummyModule";
        ObjectWarning("No modules selected; inserting a DummyModule");

    }

    //mods["Http"] = "SkWebServer";

    modsManager = new SkModulesManager(this);
    modsManager->setMode(SkThreadsPoolMode::TP_SEQUENCIAL);
    modsManager->setObjectName("Manager");

    Attach(modsManager, started, this, onModsManagerStart, SkDirect);//SkDirect);
    Attach(modsManager, finished, this, onModsManagerStop, SkQueued);
    Attach(modsManager, moduleThreadInstanced, this, onModThreadInstanced, SkQueued);//SkDirect);

    Attach(modsManager, moduleTickAvgChecked, this, onModTickAvgCheck, SkQueued);//SkDirect);
    Attach(modsManager, appTickAvgChanged, this, onAppTickAvgChange, SkQueued);//SkDirect);
    Attach(modsManager, appTickAdjusted, this, onAppTickAdjust, SkQueued);//SkDirect);

    eventLoop()->performAttachs();

    if (!modsManager->init(mods, token.c_str(), cfg->value("checkTickTime").toDouble()))
        return false;

    /*if (preBuiltMods)
        for(uint64_t i=0; i<preBuiltMods->count(); i++)
            modsManager->add(preBuiltMods->at(i));*/

    if(modsManager->isEmpty())
        return false;

    for(uint64_t i=0; i<modsManager->count(); i++)
    {
        SkModuleThread *mTh = DynCast(SkModuleThread, modsManager->getModuleThread(i));
        SkAbstractModuleCfg *modConfig = mTh->config();

        modConfig->setFlowServerPath(flowService->svr()->getDeliveryLocalPath());
    }

    modsManager->start();
    return true;
}

void ModulesCtrlService::onQuit()
{
    uint64_t last = modsManager->count()-1;
    SkModuleThread *mTh = modsManager->getModuleThread(last);
    mTh->thEventLoop()->invokeSlot(mTh->module()->quit_SLOT, mTh->module(), nullptr);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(ModulesCtrlService, onModsManagerStart)
{
    SilentSlotArgsWarning();

    ObjectWarning("Modules setup ..");

    for(uint64_t i=0; i<modsManager->count(); i++)
    {
        SkModuleThread *mTh = modsManager->getModuleThread(i);

        if (mTh->config()->getParameter("enableOnStart")->value().toBool())
        {
            SkArgsMap m;
            m["changeRunningState"] = true;

            SkWorkerTransaction *t = new SkWorkerTransaction(owner);
            t->setCommand(mTh->module(), nullptr, owner->onWrkEvaluatedCmd_SLOT, "changeRunningState", &m);
            t->invoke();
        }

        else
            ObjectWarning_EXT(mTh, "Module is NOT enabled on start by config [enableOnStart: false]");

        Attach(mTh->module(), quitted, this, onModuleQuitted, SkQueued);
    }

    ObjectWarning("Modules are READY now");
    enabled();
}

SlotImpl(ModulesCtrlService, onModsManagerStop)
{
    SilentSlotArgsWarning();

    for(uint64_t i=0; i<modsManager->count(); i++)
    {
        SkModuleThread *mTh = modsManager->getModuleThread(i);
        mTh->destroyLater();
    }

    modsManager->destroyLater();
    modsManager = nullptr;

    flowService->quit();
    disabled();

    ObjectWarning("Modules are QUITTED now");
}

SlotImpl(ModulesCtrlService, onModThreadInstanced)
{
    SilentSlotArgsWarning();

    //DEPRECATED
    /*SkModuleThread *mTh = DynCast(SkModuleThread, referer);
    flowService->svr()->addPairDatabase(mTh->moduleName());*/
}

SlotImpl(ModulesCtrlService, onModTickAvgCheck)
{
    SilentSlotArgsWarning();

    SkModuleThread *mTh = DynCast(SkModuleThread, referer);
    SkFlowPairsDatabase *db = flowService->svr()->getPairDatabase(mTh->moduleName());

    if (!db)
    {
        ObjectWarning("CANNOT get ModulePairDB: " << mTh->moduleName());
        return;
    }

    int64_t avgTime = Arg_Int64_REAL;
    int64_t consumedAvgTime = Arg_Int64_REAL;
    float jobLoadAvg = (float) consumedAvgTime / avgTime;

    db->setVariable("tickAvg", avgTime/1000.f);
    db->setVariable("consumedAvg", consumedAvgTime/1000.f);
    db->setVariable("jobLoadAvg", jobLoadAvg);
}

SlotImpl(ModulesCtrlService, onModuleQuitted)
{
    SilentSlotArgsWarning();

    quittedModules++;

    if (quittedModules == modsManager->count())
    {
        quittedModules = 0;
        eventLoop()->invokeSlot(modsManager->quit_SLOT, modsManager, nullptr);
    }

    else
    {
        uint64_t current = modsManager->count()-1-quittedModules;
        SkModuleThread *mTh = modsManager->getModuleThread(current);
        mTh->thEventLoop()->invokeSlot(mTh->module()->quit_SLOT, mTh->module(), nullptr);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(ModulesCtrlService, onAppTickAvgChange)
{
    SilentSlotArgsWarning();

    SkFlowPairsDatabase *mainDB = flowService->main();

    if (!mainDB)
        return;

    int64_t tickTimeAverageUS_HOLD = Arg_Int64_REAL;
    int64_t consumedTimeAverageUS_HOLD = Arg_Int64_REAL;
    float jobLoadAvg = (float) consumedTimeAverageUS_HOLD / tickTimeAverageUS_HOLD;

    mainDB->setVariable("tickAvg", tickTimeAverageUS_HOLD/1000.f);
    mainDB->setVariable("consumedAvg", consumedTimeAverageUS_HOLD/1000.f);
    mainDB->setVariable("jobLoadAvg", jobLoadAvg);
}

SlotImpl(ModulesCtrlService, onAppTickAdjust)
{
    SilentSlotArgsWarning();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkModulesManager *ModulesCtrlService::manager()
{
    return modsManager;
}

SkArgsMap &ModulesCtrlService::modules()
{
    return mods;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

//DEPRECATED
/*void ModulesCtrlService::onModCfgNotFound(SkAbstractModuleCfg *modCfg, SkArgsMap &defaultCfg)
{
    SkString json;
    defaultCfg.toString(json, true, 2);
    cout << json.c_str() << "\n";

    ObjectMessage_EXT(modCfg, "Do you want change the default module-configuration now?");
    cout << "[y/n]: "; cout.flush();

    SkString s;
    cin >> s;

    if (s == "n")
        return;

    SkStringList keys;
    defaultCfg.keys(keys);

    //
    s = "c";

    while(s == "c")
    {
        s.clear();

        for(uint64_t i=0; i<keys.count(); i++)
        {
            SkWorkerParam *p = modCfg->getParameter(keys[i].c_str());
            cout << i << ". " << keys[i] << " -> " << p->description() << "\n";
        }

        ObjectMessage_EXT(modCfg, "Select the configuration-key you want to change the value [write 'n' to go next]");
        cout << "Valid indexes are [0 .. " << (keys.count()-1) << "]: "; cout.flush();

        uint64_t i;
        cin >> i;

        if (i == 'n')
            return;

        if (i < keys.count())
        {
            SkWorkerParam *p = modCfg->getParameter(keys[i].c_str());
            cout << keys[i] << " (" << p->description() << "): "; cout.flush();

            cin >> s;

            if (SkVariant::isBoolean(p->valueType()))
                defaultCfg[keys[i]] = static_cast<bool>(s.toInt());

            else if (SkVariant::isInteger(p->valueType()))
                defaultCfg[keys[i]] = s.toInt();

            else if (SkVariant::isReal(p->valueType()))
                defaultCfg[keys[i]] = s.toDouble();

            else if (SkVariant::isMap(p->valueType()) || SkVariant::isList(p->valueType()))
            {
                if (!defaultCfg[keys[i]].fromJson(s.c_str()))
                {
                    ObjectError_EXT(modCfg, "json document is NOT valid");
                }
            }

            else
                defaultCfg[keys[i]] = s;

            SkString json;
            defaultCfg.toString(json, true, 2);
            ObjectMessage_EXT(modCfg, "It is new configuration: " << json);;
            cout << json.c_str() << "\n";
        }

        else
            ObjectError_EXT(modCfg, "Must select an valid index [0 .. " << (keys.count()-1) << "]: " << i);

        ObjectMessage_EXT(modCfg, "Do you want continue to configure this module?");
        cout << "[y/n]: "; cout.flush();

        cin >> s;

        if (s == "y")
            s = "c";
    }
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
