#ifndef ROBOTSENSORMETER_H
#define ROBOTSENSORMETER_H

#include <Core/App/skapp.h>
#include <Core/System/Network/FlowNetwork/skflowasync.h>

#include <UI/FLTK/skfltkui.h>
#include <UI/FLTK/skfltkwindows.h>
#include <UI/FLTK/skfltkbuttons.h>
#include <UI/FLTK/skfltkmenus.h>
#include <UI/FLTK/skfltkviews.h>
#include <UI/FLTK/skfltkindicators.h>
#include <UI/FLTK/skfltklcddisplay.h>
#include <UI/FLTK/skfltkgauge.h>

class RobotSensorMeter extends SkFlowAsync
{
    public:
        Constructor(RobotSensorMeter, SkFlowAsync);

        Slot(init);
        Slot(inputData);
        Slot(quit);

        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);

        Slot(fullScreen);

        Slot(testToggleClick);
        Slot(testPulseClick);

        Slot(onDisconnection);

    private:
        SkFltkDoubleWindow *window;
        SkFltkLed *rx;
        SkFltkLed *tx;

        SkFltkLcdDisplay *lcdDisplay;
        SkFltkGauge *gauge;
        SkFltkLed *led;

        void buildCLI(SkStringList &acceptedKeys);
        void buildUI();
        void buildASync();
        void closeApplication();

        void onChannelAdded(SkFlowChanID chanID)    override;
        void onChannelRemoved(SkFlowChanID chanID)  override;

        void onInputSetup(SkFlowChannel *ch);
        void onFlowDataCome();
};

#endif // ROBOTSENSORMETER_H
