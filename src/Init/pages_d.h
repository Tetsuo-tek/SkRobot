#ifndef PAGES_D_H
#define PAGES_D_H

const char *pages_d_rootdir = "pages.d";
const unsigned pages_d_count = 28;

const struct {
	const bool isDir;
	const char *relativePath;
	const char *name;
	const unsigned size;
	const char *data;
} pages_d[28] = {
	{
		true,
		"pages.d/",
		"Main.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"VideoSource.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"Setup.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"MotionDetect.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"AudioCapture.page",
		0,
		nullptr
	},
	{
		false,
		"pages.d/",
		"menu.json",
		191,
		"{\n" \
		"    \"Application\" : [\"Setup\"],\n" \
		"    \"View\" : [\"System\"],\n" \
		"    \"Modules\" : [\"Camera\", \"MotionDetect\", \"FaceDetect\", \"VideoSource\", \"AudioCapture\", \"Spectrogram\"],\n" \
		"    \"TestPages\" : [\"Test\"]\n" \
		"}\n"
	},
	{
		true,
		"pages.d/",
		"Camera.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"FaceDetect.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"Test.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"Spectrogram.page",
		0,
		nullptr
	},
	{
		true,
		"pages.d/",
		"System.page",
		0,
		nullptr
	},
	{
		false,
		"pages.d/VideoSource.page/",
		"wui.json",
		2808,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-3'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"components/image.html\",\n" \
		"                                \"defaultsForVariables\" : {\"SRC\" : \"/$$appName$$/VideoSource.JPeg.multipart\"}\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"VideoSource\",\n" \
		"                            \"key\" : \"changeRunningState_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"VideoSource\",\n" \
		"                            \"key\" : \"flipFrame_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"VideoSource\",\n" \
		"                            \"key\" : \"toggleMonochromatic_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-9'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"VideoSource\",\n" \
		"                            \"key\" : \"changeConfig_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"VideoSource\",\n" \
		"                            \"key\" : \"whiteBalancer_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/MotionDetect.page/",
		"wui.json",
		1963,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-3'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"components/image.html\",\n" \
		"                                \"defaultsForVariables\" : {\"SRC\" : \"/$$appName$$/MotionDetect.JPeg.multipart\"}\n" \
		"                            }\n" \
		"                        }\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-9'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"MotionDetect\",\n" \
		"                            \"key\" : \"changeConfig_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/Camera.page/",
		"wui.json",
		2778,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-3'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"components/image.html\",\n" \
		"                                \"defaultsForVariables\" : {\"SRC\" : \"/$$appName$$/Camera.JPeg.multipart\"}\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Camera\",\n" \
		"                            \"key\" : \"changeRunningState_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Camera\",\n" \
		"                            \"key\" : \"flipFrame_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Camera\",\n" \
		"                            \"key\" : \"toggleMonochromatic_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-9'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Camera\",\n" \
		"                            \"key\" : \"changeConfig_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Camera\",\n" \
		"                            \"key\" : \"whiteBalancer_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/Test.page/",
		"wui.json",
		11388,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"title\" : \"Test FORM container\",\n" \
		"            \"id\" : \"TEST_FORM_CONTAINER\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"title\" : \"Test command\",\n" \
		"                    \"id\" : \"FORM\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"form/control.html\",\n" \
		"\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TITLE\" : \"Title\",\n" \
		"                            \"DESC\" : \"This is a test CTRL form\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-switch\",\n" \
		"                            \"id\" : \"FORM_SWITCH\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-switch.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"CHECKED\" : \"checked\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-check\",\n" \
		"                            \"id\" : \"FORM_CHECK\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-check.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-textline\",\n" \
		"                            \"id\" : \"FORM_TXTLINE\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-textline.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"PLACEHOLDER\" : \"This is a placeholder\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-password\",\n" \
		"                            \"id\" : \"FORM_PASSOWRD\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-password.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"PLACEHOLDER\" : \"This is a placeholder\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-email\",\n" \
		"                            \"id\" : \"FORM_EMAIL\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-email.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"PLACEHOLDER\" : \"This is a placeholder\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-textarea\",\n" \
		"                            \"id\" : \"FORM_TEXTAREA\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-textarea.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"PLACEHOLDER\" : \"This is a placeholder\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-slider\",\n" \
		"                            \"id\" : \"FORM_SLIDER\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-slider.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-file\",\n" \
		"                            \"id\" : \"FORM_FILE\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-file.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"MULTIPLE\" : \"multiple\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-directory\",\n" \
		"                            \"id\" : \"FORM_DIR\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-directory.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"MULTIPLE\" : \"multiple\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-colorpicker\",\n" \
		"                            \"id\" : \"FORM_COLORPICKER\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-colorpicker.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-select combo\",\n" \
		"                            \"id\" : \"FORM_SELECT1\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-select.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            },\n" \
		"                            \"widgets\" :\n" \
		"                            [\n" \
		"                                {\n" \
		"                                    \"type\" : \"Container\",\n" \
		"                                    \"title\" : \"Test input-option1\",\n" \
		"                                    \"id\" : \"FORM_OPT1\",\n" \
		"                                    \"html\" :\n" \
		"                                    {\n" \
		"                                        \"template\" : \"form/input-option.html\",\n" \
		"\n" \
		"                                        \"defaultsForVariables\" :\n" \
		"                                        {\n" \
		"                                            \"VALUE\" : \"1\"\n" \
		"                                        }\n" \
		"                                    }\n" \
		"                                },\n" \
		"                                {\n" \
		"                                    \"type\" : \"Container\",\n" \
		"                                    \"title\" : \"Test input-option2\",\n" \
		"                                    \"id\" : \"FORM_OPT2\",\n" \
		"                                    \"html\" :\n" \
		"                                    {\n" \
		"                                        \"template\" : \"form/input-option.html\",\n" \
		"\n" \
		"                                        \"defaultsForVariables\" :\n" \
		"                                        {\n" \
		"                                            \"VALUE\" : \"2\"\n" \
		"                                        }\n" \
		"                                    }\n" \
		"                                }\n" \
		"                            ]\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Test input-select multiple\",\n" \
		"                            \"id\" : \"FORM_SELECT2\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"form/input-select.html\",\n" \
		"\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"MULTIPLE\" : \"multiple\",\n" \
		"                                    \"DESC\" : \"This is a description\"\n" \
		"                                }\n" \
		"                            },\n" \
		"                            \"widgets\" :\n" \
		"                            [\n" \
		"                                {\n" \
		"                                    \"type\" : \"Container\",\n" \
		"                                    \"title\" : \"Test input-option1\",\n" \
		"                                    \"id\" : \"FORM_OPT1\",\n" \
		"                                    \"html\" :\n" \
		"                                    {\n" \
		"                                        \"template\" : \"form/input-option.html\",\n" \
		"\n" \
		"                                        \"defaultsForVariables\" :\n" \
		"                                        {\n" \
		"                                            \"VALUE\" : \"1\"\n" \
		"                                        }\n" \
		"                                    }\n" \
		"                                },\n" \
		"                                {\n" \
		"                                    \"type\" : \"Container\",\n" \
		"                                    \"title\" : \"Test input-option2\",\n" \
		"                                    \"id\" : \"FORM_OPT2\",\n" \
		"                                    \"html\" :\n" \
		"                                    {\n" \
		"                                        \"template\" : \"form/input-option.html\",\n" \
		"\n" \
		"                                        \"defaultsForVariables\" :\n" \
		"                                        {\n" \
		"                                            \"VALUE\" : \"2\"\n" \
		"                                        }\n" \
		"                                    }\n" \
		"                                }\n" \
		"                            ]\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/System.page/",
		"networkChart.json",
		1942,
		"[\n" \
		"    {\n" \
		"        \"type\" : \"Container\",\n" \
		"        \"title\" : \"Networks stat\",\n" \
		"        \"id\" : \"ROBOT_NETWORK_CHART\",\n" \
		"        \"html\" : \n" \
		"        {\n" \
		"            \"template\" : \"chartjs/chart-container.html\",\n" \
		"            \"defaultsForVariables\":\n" \
		"            {\n" \
		"                \"ATTRIBUTES\" : \"style='min-width:100%; min-height:30vh'\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"init\" :\n" \
		"        {\n" \
		"            \"template\" : \"chartjs/init-bar-livechart.js\",\n" \
		"            \"defaultsForVariables\" :\n" \
		"            {\n" \
		"                \"LABELS\" : $$networks$$,\n" \
		"                \"MIN\" : \"0\",\n" \
		"                \"MAX\" : \"1\",\n" \
		"                \"UDM\" : \"MB/s\",\n" \
		"                \"INDEX_AXIS\" : \"y\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"tick\" :\n" \
		"        {\n" \
		"            \"interval\" : 1.0,\n" \
		"            \"template\" : \"chartjs/tick-bar-livechart.js\"\n" \
		"        },\n" \
		"        \"widgets\" :\n" \
		"        [\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"networks_tx\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-bar-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"TX\",\n" \
		"                        \"BG_COLOR\" : \"red\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-bar-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"networks_rx\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-bar-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"RX\",\n" \
		"                        \"COLOR\" : \"green\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-bar-livechart-val.js\"}\n" \
		"            }\n" \
		"        ]\n" \
		"    }\n" \
		"]\n"
	},
	{
		false,
		"pages.d/System.page/",
		"memChart.json",
		3617,
		"[\r\n" \
		"    {\r\n" \
		"        \"type\" : \"Container\",\r\n" \
		"        \"title\" : \"Memory stat\",\r\n" \
		"        \"id\" : \"ROBOT_MEMORY_CHART\",\r\n" \
		"        \"html\" : \r\n" \
		"        {\r\n" \
		"            \"template\" : \"chartjs/chart-container.html\",\r\n" \
		"            \"defaultsForVariables\":\r\n" \
		"            {\r\n" \
		"                \"ATTRIBUTES\" : \"style='min-width:100%; min-height:40vh'\"\r\n" \
		"            }\r\n" \
		"        },\r\n" \
		"        \"init\" :\r\n" \
		"        {\r\n" \
		"            \"template\" : \"chartjs/init-line-livechart.js\",\r\n" \
		"            \"defaultsForVariables\" :\r\n" \
		"            {\r\n" \
		"                \"SAMPLES_LIMIT\" : \"200\",\r\n" \
		"                \"UDM\" : \"GB\"\r\n" \
		"            }\r\n" \
		"        },\r\n" \
		"        \"tick\" :\r\n" \
		"        {\r\n" \
		"            \"interval\" : 0.5,\r\n" \
		"            \"template\" : \"chartjs/tick-line-livechart.js\"\r\n" \
		"        },\r\n" \
		"        \"widgets\" :\r\n" \
		"        [\r\n" \
		"            {\r\n" \
		"                \"type\" : \"Widget\",\r\n" \
		"                \"mode\" : \"Value\",\r\n" \
		"                \"dbName\" : \"Main\",\r\n" \
		"                \"key\" : \"ramUsed\",\r\n" \
		"                \"init\" :\r\n" \
		"                {\r\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\r\n" \
		"                    \"defaultsForVariables\" :\r\n" \
		"                    {\r\n" \
		"                        \"LABEL\" : \"Used\",\r\n" \
		"                        \"COLOR\" : \"red\"\r\n" \
		"                    }\r\n" \
		"                },\r\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\r\n" \
		"            },\r\n" \
		"            {\r\n" \
		"                \"type\" : \"Widget\",\r\n" \
		"                \"mode\" : \"Value\",\r\n" \
		"                \"dbName\" : \"Main\",\r\n" \
		"                \"key\" : \"ramCache\",\r\n" \
		"                \"init\" :\r\n" \
		"                {\r\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\r\n" \
		"                    \"defaultsForVariables\" :\r\n" \
		"                    {\r\n" \
		"                        \"LABEL\" : \"Cache\",\r\n" \
		"                        \"COLOR\" : \"orange\"\r\n" \
		"                    }\r\n" \
		"                },\r\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\r\n" \
		"            },\r\n" \
		"            {\r\n" \
		"                \"type\" : \"Widget\",\r\n" \
		"                \"mode\" : \"Value\",\r\n" \
		"                \"dbName\" : \"Main\",\r\n" \
		"                \"key\" : \"ramFree\",\r\n" \
		"                \"init\" :\r\n" \
		"                {\r\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\r\n" \
		"                    \"defaultsForVariables\" :\r\n" \
		"                    {\r\n" \
		"                        \"LABEL\" : \"Free\",\r\n" \
		"                        \"COLOR\" : \"blue\"\r\n" \
		"                    }\r\n" \
		"                },\r\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\r\n" \
		"            },\r\n" \
		"            {\r\n" \
		"                \"type\" : \"Widget\",\r\n" \
		"                \"mode\" : \"Value\",\r\n" \
		"                \"dbName\" : \"Main\",\r\n" \
		"                \"key\" : \"swapUsed\",\r\n" \
		"                \"init\" :\r\n" \
		"                {\r\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\r\n" \
		"                    \"defaultsForVariables\" :\r\n" \
		"                    {\r\n" \
		"                        \"LABEL\" : \"Used swap\",\r\n" \
		"                        \"COLOR\" : \"magenta\"\r\n" \
		"                    }\r\n" \
		"                },\r\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\r\n" \
		"            },\r\n" \
		"            {\r\n" \
		"                \"type\" : \"Widget\",\r\n" \
		"                \"mode\" : \"Value\",\r\n" \
		"                \"dbName\" : \"Main\",\r\n" \
		"                \"key\" : \"swapFree\",\r\n" \
		"                \"init\" :\r\n" \
		"                {\r\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\r\n" \
		"                    \"defaultsForVariables\" :\r\n" \
		"                    {\r\n" \
		"                        \"LABEL\" : \"Free swap\",\r\n" \
		"                        \"COLOR\" : \"brown\"\r\n" \
		"                    }\r\n" \
		"                },\r\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\r\n" \
		"            }\r\n" \
		"        ]\r\n" \
		"    }\r\n" \
		"]\r\n"
	},
	{
		false,
		"pages.d/System.page/",
		"procsChart.json",
		2997,
		"[\n" \
		"    {\n" \
		"        \"type\" : \"Container\",\n" \
		"        \"title\" : \"Processes stat\",\n" \
		"        \"id\" : \"ROBOT_PROCESSES_CHART\",\n" \
		"        \"html\" : \n" \
		"        {\n" \
		"            \"template\" : \"chartjs/chart-container.html\",\n" \
		"            \"defaultsForVariables\" : \n" \
		"            {\n" \
		"                \"ATTRIBUTES\" : \"style='min-width:100%; min-height:30vh'\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"init\" :\n" \
		"        {\n" \
		"            \"template\" : \"chartjs/init-line-livechart.js\",\n" \
		"            \"defaultsForVariables\" :\n" \
		"            {\n" \
		"                \"SAMPLES_LIMIT\" : \"150\",\n" \
		"                \"UDM\" : \"N°\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"tick\" :\n" \
		"        {\n" \
		"            \"interval\" : 0.5,\n" \
		"            \"template\" : \"chartjs/tick-line-livechart.js\"\n" \
		"        },\n" \
		"        \"widgets\" :\n" \
		"        [\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"processesRunning\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Running\",\n" \
		"                        \"COLOR\" : \"black\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"processesSleeping\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Sleeping\",\n" \
		"                        \"COLOR\" : \"magenta\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"processesStopped\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Stopped\",\n" \
		"                        \"COLOR\" : \"orange\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"processesZombie\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Zombie\",\n" \
		"                        \"COLOR\" : \"\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            }\n" \
		"        ]\n" \
		"    }\n" \
		"]\n"
	},
	{
		false,
		"pages.d/System.page/",
		"storageChart.json",
		1943,
		"[\n" \
		"    {\n" \
		"        \"type\" : \"Container\",\n" \
		"        \"title\" : \"Storage stat\",\n" \
		"        \"id\" : \"ROBOT_STORAGE_CHART\",\n" \
		"        \"html\" : \n" \
		"        {\n" \
		"            \"template\" : \"chartjs/chart-container.html\",\n" \
		"            \"defaultsForVariables\" :\n" \
		"            {\n" \
		"                \"ATTRIBUTES\" : \"style='min-width:100%; min-height:30vh'\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"init\" :\n" \
		"        {\n" \
		"            \"template\" : \"chartjs/init-bar-livechart.js\",\n" \
		"            \"defaultsForVariables\" :\n" \
		"            {\n" \
		"                \"LABELS\" : $$storages$$,\n" \
		"                \"MIN\" : \"0\",\n" \
		"                \"MAX\" : \"10\",\n" \
		"                \"UDM\" : \"MB/s\",\n" \
		"                \"INDEX_AXIS\" : \"y\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"tick\" :\n" \
		"        {\n" \
		"            \"interval\" : 1.0,\n" \
		"            \"template\" : \"chartjs/tick-bar-livechart.js\"\n" \
		"        },\n" \
		"        \"widgets\" :\n" \
		"        [\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"storages_tx\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-bar-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"TX\",\n" \
		"                        \"BG_COLOR\" : \"red\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-bar-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"storages_rx\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-bar-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"RX\",\n" \
		"                        \"COLOR\" : \"green\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-bar-livechart-val.js\"}\n" \
		"            }\n" \
		"        ]\n" \
		"    }\n" \
		"]\n"
	},
	{
		false,
		"pages.d/System.page/",
		"wui.json",
		5036,
		"{\n" \
		"    \"widgets\" : \n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"title\" : \"System Informations\",\n" \
		"            \"id\" : \"SYSINFO_TABLE\",\n" \
		"            \"html\" : \n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" : \n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='container-lg'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"title\" : \"System Informations\",\n" \
		"                    \"id\" : \"CPU_MEM_INFO_ROW\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" : \n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"CPU\",\n" \
		"                            \"id\" : \"SYSINFO_CPU_COL\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"container.html\",\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"TAG\" : \"div\",\n" \
		"                                    \"ATTRIBUTES\" : \"class='col-lg-6'\"\n" \
		"                                }\n" \
		"                            },\n" \
		"                            \"widgetsFilePath\" : \"cpuChart.json\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Memory\",\n" \
		"                            \"id\" : \"SYSINFO_MEM_COL\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"container.html\",\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"TAG\" : \"div\",\n" \
		"                                    \"ATTRIBUTES\" : \"class='col-lg-6'\"\n" \
		"                                }\n" \
		"                            },\n" \
		"                            \"widgetsFilePath\" : \"memChart.json\"\n" \
		"                        }\n" \
		"\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"title\" : \"System Informations\",\n" \
		"                    \"id\" : \"NETANDPROCS_ROW\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" : \n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Network\",\n" \
		"                            \"id\" : \"NETWORK_COL\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"container.html\",\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"TAG\" : \"div\",\n" \
		"                                    \"ATTRIBUTES\" : \"class='col-lg-4'\"\n" \
		"                                }\n" \
		"                            },\n" \
		"                            \"widgetsFilePath\" : \"networkChart.json\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Storage\",\n" \
		"                            \"id\" : \"STORAGE_COL\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"container.html\",\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"TAG\" : \"div\",\n" \
		"                                    \"ATTRIBUTES\" : \"class='col-lg-4'\"\n" \
		"                                }\n" \
		"                            },\n" \
		"                            \"widgetsFilePath\" : \"storageChart.json\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"title\" : \"Processes\",\n" \
		"                            \"id\" : \"PROCS_COL\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"container.html\",\n" \
		"                                \"defaultsForVariables\" :\n" \
		"                                {\n" \
		"                                    \"TAG\" : \"div\",\n" \
		"                                    \"ATTRIBUTES\" : \"class='col-lg-4'\"\n" \
		"                                }\n" \
		"                            },\n" \
		"                            \"widgetsFilePath\" : \"procsChart.json\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/System.page/",
		"cpuChart.json",
		4043,
		"[\n" \
		"    {\n" \
		"        \"type\" : \"Container\",\n" \
		"        \"title\" : \"CPU time stat\",\n" \
		"        \"id\" : \"ROBOT_CPU_CHART\",\n" \
		"        \"html\" : \n" \
		"        {\n" \
		"            \"template\" : \"chartjs/chart-container.html\",\n" \
		"            \"defaultsForVariables\":\n" \
		"            {\n" \
		"                \"ATTRIBUTES\" : \"style='min-width:100%; min-height:40vh'\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"init\" :\n" \
		"        {\n" \
		"            \"template\" : \"chartjs/init-line-livechart.js\",\n" \
		"            \"defaultsForVariables\" :\n" \
		"            {\n" \
		"                \"SAMPLES_LIMIT\" : \"200\",\n" \
		"                \"UDM\" : \"%\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"tick\" :\n" \
		"        {\n" \
		"            \"interval\" : 0.5,\n" \
		"            \"template\" : \"chartjs/tick-line-livechart.js\"\n" \
		"        },\n" \
		"        \"widgets\" :\n" \
		"        [\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"userTime\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"User\",\n" \
		"                        \"COLOR\" : \"blue\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"kernelTime\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Kernel\",\n" \
		"                        \"COLOR\" : \"black\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"ioTime\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"IO\",\n" \
		"                        \"COLOR\" : \"red\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"swapTime\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Swap\",\n" \
		"                        \"COLOR\" : \"orange\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"niceTime\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Nice\",\n" \
		"                        \"COLOR\" : \"magenta\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"idleTime\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Idle\",\n" \
		"                        \"COLOR\" : \"cyan\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            }\n" \
		"        ]\n" \
		"    }\n" \
		"]\n"
	},
	{
		false,
		"pages.d/Main.page/",
		"tickAvgPairs.json",
		7018,
		"[\n" \
		"    {\n" \
		"        \"type\" : \"Container\",\n" \
		"        \"title\" : \"Tick intervals average\",\n" \
		"        \"html\" : \n" \
		"        {\n" \
		"            \"template\" : \"components/pair-list.html\"\n" \
		"        },\n" \
		"        \"widgets\" :\n" \
		"        [\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Http\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"Http\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Main\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"$$appName$$\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Speaker\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"Speaker\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Camera\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"Camera\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"VideoSource\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"VideoSource\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"MotionDetect\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"MotionDetect\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"CodeDetect\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"CodeDetect\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"FaceDetect\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"FaceDetect\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"FaceRecognize\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"FaceRecognize\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"AudioCapture\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"AudioCapture\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"OgaEncoder\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"OgaEncoder\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"Spectrogram\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"Spectrogram\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Value\",\n" \
		"                \"dbName\" : \"ThermoCap\",\n" \
		"                \"key\" : \"tickAvg\",\n" \
		"                \"label\" : \"ThermoCap\",\n" \
		"                \"udm\" : \"msec\",\n" \
		"                \"html\" :\n" \
		"                {\n" \
		"                    \"template\" : \"components/pair-list-item.html\",\n" \
		"                    \"defaultsForVariables\" : {\"OPT_CLASSES_VALUE\" : \"text-end\"}\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"jquery/element-set-value.js\"}\n" \
		"            }\n" \
		"        ]\n" \
		"    }\n" \
		"]\n"
	},
	{
		false,
		"pages.d/Main.page/",
		"tickAvgChart.json",
		6600,
		"[\n" \
		"    {\n" \
		"        \"type\" : \"Container\",\n" \
		"        \"title\" : \"Tick times average\",\n" \
		"        \"id\" : \"ROBOT_TICKAVG_CHART\",\n" \
		"        \"html\" :\n" \
		"        {\n" \
		"            \"template\" : \"chartjs/chart-container.html\",\n" \
		"            \"defaultsForVariables\":\n" \
		"            {\n" \
		"                \"ATTRIBUTES\" : \"class='box' style='min-width: 100%; height:50vh'\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"init\" :\n" \
		"        {\n" \
		"            \"template\" : \"chartjs/init-line-livechart.js\",\n" \
		"            \"defaultsForVariables\" :\n" \
		"            {\n" \
		"                \"SAMPLES_LIMIT\" : \"200\",\n" \
		"                \"UDM\" : \"msec\"\n" \
		"            }\n" \
		"        },\n" \
		"        \"tick\" :\n" \
		"        {\n" \
		"            \"interval\" : 0.4,\n" \
		"            \"template\" : \"chartjs/tick-line-livechart.js\"\n" \
		"        },\n" \
		"        \"widgets\" :\n" \
		"        [\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"Main_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"$$appName$$\",\n" \
		"                        \"COLOR\" : \"blue\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"Http_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Http\",\n" \
		"                        \"COLOR\" : \"brown\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"Camera_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Camera\",\n" \
		"                        \"COLOR\" : \"red\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"VideoSource_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"VideoSource\",\n" \
		"                        \"COLOR\" : \"red\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"MotionDetect_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"MotionDetect\",\n" \
		"                        \"COLOR\" : \"orange\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"CodeDetect_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"CodeDetect\",\n" \
		"                        \"COLOR\" : \"cyan\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"FaceDetect_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"FaceDetect\",\n" \
		"                        \"COLOR\" : \"magenta\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"FaceRecognize_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"FaceRecognize\",\n" \
		"                        \"COLOR\" : \"brown\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"AudioCapture_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"AudioCapture\",\n" \
		"                        \"COLOR\" : \"green\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"OgaEncoder_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"OgaEncoder\",\n" \
		"                        \"COLOR\" : \"darkgreen\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            },\n" \
		"            {\n" \
		"                \"type\" : \"Widget\",\n" \
		"                \"mode\" : \"Target\",\n" \
		"                \"id\" : \"Spectrogram_tickAvg\",\n" \
		"                \"init\" :\n" \
		"                {\n" \
		"                    \"template\" : \"chartjs/init-line-livechart-val.js\",\n" \
		"                    \"defaultsForVariables\" :\n" \
		"                    {\n" \
		"                        \"LABEL\" : \"Spectrogram\",\n" \
		"                        \"COLOR\" : \"orange\"\n" \
		"                    }\n" \
		"                },\n" \
		"                \"run\" : {\"template\" : \"chartjs/run-line-livechart-val.js\"}\n" \
		"            }\n" \
		"        ]\n" \
		"    }\n" \
		"]\n"
	},
	{
		false,
		"pages.d/Main.page/",
		"wui.json",
		1738,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Pairs\",\n" \
		"            \"dbName\" : \"Main\",\n" \
		"            \"key\" : \"tickAvgs\",\n" \
		"            \"udm\" : \"msec\"\n" \
		"        },\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"title\" : \"Tick times average\",\n" \
		"            \"id\" : \"TICKAVG_ROW\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"title\" : \"Values\",\n" \
		"                    \"id\" : \"TICKAVG_PAIRS_COL\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-3'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgetsFilePath\" : \"tickAvgPairs.json\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"title\" : \"Charts\",\n" \
		"                    \"id\" : \"TICKAVG_CHART_COL\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-9'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgetsFilePath\" : \"tickAvgChart.json\"\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/AudioCapture.page/",
		"wui.json",
		2612,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-3'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"AudioCapture\",\n" \
		"                            \"key\" : \"setMute_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"AudioCapture\",\n" \
		"                            \"key\" : \"setGain_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"AudioCapture\",\n" \
		"                            \"key\" : \"setBass_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"AudioCapture\",\n" \
		"                            \"key\" : \"setMiddle_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"AudioCapture\",\n" \
		"                            \"key\" : \"setTreble_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-9'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"AudioCapture\",\n" \
		"                            \"key\" : \"changeConfig_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/Spectrogram.page/",
		"wui.json",
		3022,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-3'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"components/image.html\",\n" \
		"                                \"defaultsForVariables\" : {\"SRC\" : \"/$$appName$$/Spectrogram.JPeg.multipart\"}\n" \
		"                            }\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Spectrogram\",\n" \
		"                            \"key\" : \"changeRunningState_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Spectrogram\",\n" \
		"                            \"key\" : \"setFalseColorsMap_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Spectrogram\",\n" \
		"                            \"key\" : \"flipFrame_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Spectrogram\",\n" \
		"                            \"key\" : \"toggleMonochromatic_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-9'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Spectrogram\",\n" \
		"                            \"key\" : \"changeConfig_TRANS\"\n" \
		"                        },\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"Spectrogram\",\n" \
		"                            \"key\" : \"whiteBalancer_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/Setup.page/",
		"wui.json",
		557,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Form\",\n" \
		"                    \"dbName\" : \"Main\",\n" \
		"                    \"key\" : \"changeConfig_TRANS\"\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	},
	{
		false,
		"pages.d/FaceDetect.page/",
		"wui.json",
		1959,
		"{\n" \
		"    \"widgets\" :\n" \
		"    [\n" \
		"        {\n" \
		"            \"type\" : \"Container\",\n" \
		"            \"html\" :\n" \
		"            {\n" \
		"                \"template\" : \"container.html\",\n" \
		"                \"defaultsForVariables\" :\n" \
		"                {\n" \
		"                    \"TAG\" : \"div\",\n" \
		"                    \"ATTRIBUTES\" : \"class='row'\"\n" \
		"                }\n" \
		"            },\n" \
		"            \"widgets\" :\n" \
		"            [\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-3'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Container\",\n" \
		"                            \"html\" :\n" \
		"                            {\n" \
		"                                \"template\" : \"components/image.html\",\n" \
		"                                \"defaultsForVariables\" : {\"SRC\" : \"/$$appName$$/FaceDetect.JPeg.multipart\"}\n" \
		"                            }\n" \
		"                        }\n" \
		"                    ]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"type\" : \"Container\",\n" \
		"                    \"html\" :\n" \
		"                    {\n" \
		"                        \"template\" : \"container.html\",\n" \
		"                        \"defaultsForVariables\" :\n" \
		"                        {\n" \
		"                            \"TAG\" : \"div\",\n" \
		"                            \"ATTRIBUTES\" : \"class='col-md-9'\"\n" \
		"                        }\n" \
		"                    },\n" \
		"                    \"widgets\" :\n" \
		"                    [\n" \
		"                        {\n" \
		"                            \"type\" : \"Form\",\n" \
		"                            \"dbName\" : \"FaceDetect\",\n" \
		"                            \"key\" : \"changeConfig_TRANS\"\n" \
		"                        }\n" \
		"                    ]\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	}
};

#define PAGES_D_INDEX	"{\"pages.d/AudioCapture.page\":4,\"pages.d/AudioCapture.page/wui.json\":24,\"pages.d/Camera.page\":6,\"pages.d/Camera.page/wui.json\":13,\"pages.d/FaceDetect.page\":7,\"pages.d/FaceDetect.page/wui.json\":27,\"pages.d/Main.page\":0,\"pages.d/Main.page/tickAvgChart.json\":22,\"pages.d/Main.page/tickAvgPairs.json\":21,\"pages.d/Main.page/wui.json\":23,\"pages.d/MotionDetect.page\":3,\"pages.d/MotionDetect.page/wui.json\":12,\"pages.d/Setup.page\":2,\"pages.d/Setup.page/wui.json\":26,\"pages.d/Spectrogram.page\":9,\"pages.d/Spectrogram.page/wui.json\":25,\"pages.d/System.page\":10,\"pages.d/System.page/cpuChart.json\":20,\"pages.d/System.page/memChart.json\":16,\"pages.d/System.page/networkChart.json\":15,\"pages.d/System.page/procsChart.json\":17,\"pages.d/System.page/storageChart.json\":18,\"pages.d/System.page/wui.json\":19,\"pages.d/Test.page\":8,\"pages.d/Test.page/wui.json\":14,\"pages.d/VideoSource.page\":1,\"pages.d/VideoSource.page/wui.json\":11,\"pages.d/menu.json\":5}"

#endif // PAGES_D_H
