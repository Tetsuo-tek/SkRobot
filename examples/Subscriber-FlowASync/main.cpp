#include "subscriber.h"
SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    skApp->init(5000, 200000, SK_TIMEDLOOP_SLEEPING);

    Subscriber *a = new Subscriber;
    Attach(skApp->started_SIG, pulse, a, init, SkOneShotQueued);

    return skApp->exec();
}
