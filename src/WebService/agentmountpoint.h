#ifndef AGENTMOUNTPOINT_H
#define AGENTMOUNTPOINT_H

#if defined(UMBA)

#include <Core/System/Network/FlowNetwork/skflowasync.h>
#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skgenericmountpoint.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class  WebAgentConnection extends SkFlowAsync
{
    SkWebSocket *sck;

    SkString agentSpeakingPcmChanName;
    SkFlowChannel *agentSpeakingPcmChan;

    SkString faceBoxesChanName;
    SkFlowChannel *faceBoxesChan;

    SkString faceIDChanName;
    SkFlowChannel *faceIDChan;

    SkFlowChanID camChan;
    SkFlowChanID micChan;

    public:
        Constructor(WebAgentConnection, SkFlowAsync);

        void setup(SkWebSocket *socket);

        Slot(onTextReadyRead);
        Slot(onDataReadyRead);
        Slot(onDisconnected);

        Slot(onFastTick);
        Slot(onSlowTick);
        Slot(onOneSecondTick);

    private:
        void onChannelAdded(SkFlowChanID)                               override;
        void onChannelRemoved(SkFlowChanID)                             override;

        void onFlowDataCome(SkFlowChanID chanID, void *data, UInt sz);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkHttpService;

class WebAgentMountpoint extends SkGenericMountPoint
{
    SkString svrName;
    SkString userName;
    SkString svrPath;
    SkString token;
    WebAgentConnection *conn;

    public:
        Constructor(WebAgentMountpoint, SkGenericMountPoint);

        bool init(CStr *serverName, CStr *fsPath, CStr *fsUser, CStr *fsToken);
        void close();

        Slot(onUserAccepted);
        Slot(onUserDisconnected);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif

#endif // AGENTMOUNTPOINT_H
