#include "robotmonitor.h"
#include "Core/System/skosenv.h"

ConstructorImpl(RobotMonitor, SkFlowAsync)
{
    pairsChanName = "Pairs";
    pairsChan = -1;

    eventsChanName = "Events";
    eventsChan = -1;

    jsonOutput = osEnv()->existsAppArgument("--json");

    SlotSet(init);
    SlotSet(inputData);
    SlotSet(onDisconnection);
    SlotSet(exitFromKernelSignal);

    Attach(this, channelDataAvailable, this, inputData, SkDirect);
    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);
    Attach(this, disconnected, this, onDisconnection, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotMonitor, init)
{
    SilentSlotArgsWarning();

    SkStringList acceptedKeys;
    buildCLI(acceptedKeys);
    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));

    if (osEnv()->existsAppArgument("--help"))
    {
        cout << osEnv()->cmdLineHelp();
        skApp->quit();
        return;
    }

    buildASync();

    logger->enableOwnerName(false);
    logger->enable(true);
}

void RobotMonitor::buildASync()
{
    AssertKiller(!osEnv()->existsEnvironmentVar("ROBOT_ADDRESS"));
    SkString robotAddress = osEnv()->getEnvironmentVar("ROBOT_ADDRESS");

    if (robotAddress.startsWith("local:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address [$ROBOT_ADDRESS]: " << robotAddress);

        AssertKiller(!localConnect(robotAddress.c_str()));
    }

    else if (robotAddress.startsWith("tcp:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address [$ROBOT_ADDRESS]: " << robotAddress);

        SkStringList robotAddressParsed;
        robotAddress.split(":", robotAddressParsed);
        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);

        if (robotAddressParsed.count() == 1)
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));

        else
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));
    }

    SkString userName = "guest";
    SkString userPasswd = "password";

    /*cout << "\nRobot login:\nUsername: ";
    cin >> userName;
    cout << "Password: ";
    SkStdInput::unSetTermAttr(ECHO);
    cin >> userPasswd;
    SkStdInput::setTermAttr(ECHO);*/

    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotMonitor::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (ch->name == pairsChanName)
    {
        ObjectMessage("Pairs channel is READY");

        pairsChan = ch->chanID;
        subscribeChannel(ch);
    }

    else if (ch->name == eventsChanName)
    {
        ObjectMessage("Events channel is READY");

        eventsChan = ch->chanID;
        subscribeChannel(ch);
    }
}

void RobotMonitor::onChannelRemoved(SkFlowChanID chanID)
{
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //



//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotMonitor, inputData)
{
    SilentSlotArgsWarning();

    while(nextData())
    {
        SkFlowChannelData &d = getCurrentData();

        if (d.chanID == pairsChan)
        {
            SkString json = d.data.toString();

            if (jsonOutput)
                ObjectMessage(json);

            else
            {
                SkVariant v;
                v.fromJson(json.c_str());
                SkStringList values;
                v.copyToStringList(values);

                SkString &cmd = values.first();
                SkString &db = values.last();
                SkString &key = values[1];
                SkString &val = values[2];

                ObjectMessage("Pair: " << cmd << " " << db << "." << key << " -> " << val);
            }
        }

        else if (d.chanID == eventsChan)
        {
            SkString json = d.data.toString();

            if (jsonOutput)
                ObjectWarning(json);

            else
            {
                SkVariant v;
                v.fromJson(json.c_str());
                SkTreeMap<SkString, SkVariant> values;
                v.copyToMap(values);

                SkString loopName;
                SkString ownerName;
                SkString owner_T;
                SkString evtFamily;
                SkString evtName;
                SkString evtParams;

                SkBinaryTreeVisit<SkString, SkVariant> *itr = values.iterator();

                while(itr->next())
                {
                    SkString &k = itr->item().key();
                    SkVariant &v = itr->item().value();

                    if (k == "loop")
                        loopName = v.toString();

                    else if (k == "owner")
                        ownerName = v.toString();

                    else if (k == "owner_T")
                        owner_T = v.toString();

                    else if (k == "family")
                        evtFamily = v.toString();

                    else if (k == "name")
                        evtName = v.toString();

                    else if (k == "values")
                        evtParams = v.toString();
                }

                delete itr;

                ObjectWarning("Event: " << loopName << "::" << ownerName << "{" << owner_T << "} -> " << evtFamily << "." << evtName << "(" << evtParams << ")");
            }
        }
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotMonitor, onDisconnection)
{
    SilentSlotArgsWarning();
}

SlotImpl(RobotMonitor, exitFromKernelSignal)
{
    SilentSlotArgsWarning();

    if (Arg_Int != SIGINT)
        return;

    ObjectMessage("Forcing application exit with CTRL+C");
    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotMonitor::buildCLI(SkStringList &acceptedKeys)
{
    acceptedKeys.append("--help");
    acceptedKeys.append("--json");

    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

    osEnv()->addCliAppHelp("--help", "prints this output and exit");
    osEnv()->addCliAppHelp("--json", "sets output as JSON not indented");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
