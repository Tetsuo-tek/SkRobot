#include "audiocap.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    SkCli *cli = skApp->appCli();

    cli->add("--device",        "-d", "default",    "Setup the capture device");
    cli->add("--buffer-frames", "-b", "1024",       "Setup the audio frames per channel");
    cli->add("--channels",      "-c", "1",          "Setup channels (interleaved)");
    cli->add("--format",        "-f", "SHORT",      "Setup the sample type (UCHAR, SHORT, INT, FLOAT)");
    cli->add("--sample-rate",   "-r", "48000",      "Setup the capturing sample rate");

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new AudioCap;

    return skApp->exec();
}
