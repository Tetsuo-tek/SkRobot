#include "init.h"
#include "../physicalbody.h"

#include "pages_d.h"
#include "users_d.h"
#include "templates.h"
#include "www.h"

#include <Core/System/Filesystem/skfsutils.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(Init, SkAbstractWorkerObject)
{
    owner = dynamic_cast<PhysicalBody *>(parent());
    setObjectName("Init");

    config = owner->cfg();

    safeMode = false;
    started = false;
    quitting = false;

    token = uniqueID();

    SlotSet(onModulesStarted);
    SlotSet(onModulesStopped);

    SlotSet(oneSecTick);
    SlotSet(onShutdown);

    SignalSet(ready);

    flowService = new FlowService(this);
    modCtrlService = new ModulesCtrlService(this);
    modCtrlService->setup(flowService, token.c_str());

    Attach(modCtrlService, enabled, this, onModulesStarted, SkQueued);
    Attach(modCtrlService, disabled, this, onModulesStopped, SkQueued);

    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool Init::checkRootFs()
{
    bool ok = SkFsUtils::exists(PDB_DIR) && SkFsUtils::exists(TPL_DIR)
            && SkFsUtils::exists(WWW_DIR) && SkFsUtils::exists(PAG_DIR)
            && SkFsUtils::exists(USR_DIR) && SkFsUtils::exists(MOD_DIR);

    if (!ok)
    {
        SkString currentPath = SkFsUtils::pwd();
        ObjectWarning("Robot filesystem NOT found, do you want to use this path as working directory for SkRobot?");
        cout << currentPath << " [y/n]: "; cout.flush();

        SkString s;
        cin >> s;

        if (s == "n")
        {
            ObjectWarning("Exiting ..");
            return false;
        }

        ObjectWarning("Copying base of rootfs to: " << currentPath);

        if (!SkFsUtils::exists(PDB_DIR))
            AssertKiller(!SkFsUtils::mkdir(PDB_DIR));

        if(!SkFsUtils::exists(TPL_DIR))
            AssertKiller(!SkFsUtils::mkdir(TPL_DIR));

        SkFsUtils::copyCodeHeaderFs(templates, templates_count, currentPath.c_str(), true);

        if(!SkFsUtils::exists(WWW_DIR))
            AssertKiller(!SkFsUtils::mkdir(WWW_DIR));

        SkFsUtils::copyCodeHeaderFs(www, www_count, currentPath.c_str(), true);

        if(!SkFsUtils::exists(PAG_DIR))
            AssertKiller(!SkFsUtils::mkdir(PAG_DIR));

        SkFsUtils::copyCodeHeaderFs(pages_d, pages_d_count, currentPath.c_str(), false);

        if(!SkFsUtils::exists(USR_DIR))
            AssertKiller(!SkFsUtils::mkdir(USR_DIR));

        SkFsUtils::copyCodeHeaderFs(users_d, users_d_count, currentPath.c_str(), false);

        if (!SkFsUtils::exists(MOD_DIR))
            AssertKiller(!SkFsUtils::mkdir(MOD_DIR));

        ObjectWarning("Rootfs creation terminated");
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool Init::start()
{
    if (started)
    {
        ObjectWarning("Services are ALREADY started");
        return false;
    }

    AssertKiller(!checkRootFs());

    ObjectWarning("Starting services ..");

    if (!flowService->init(owner, config))
        return false;

    SkFlowPairsDatabase *mainDB = flowService->main();

    if (!mainDB)
    {
        ObjectError("MainDB is NOT ready");
        return false;
    }

    SkVariantList p;
    p << 1;
    skApp->propagateEvent(this, "Init", "Boot", &p);

    SkString modsWorkingDir = MOD_DIR;
    skApp->setGlobalPath("MODULES_WORKING_DIR", modsWorkingDir);

    bool ok = modCtrlService->init(owner, config);

    if (ok)
    {
        SkArgsMap &modules = modCtrlService->modules();
        mainDB->setVariable("modules", modules);
    }

    return ok;
}

void Init::respawn()
{
    if (!started)
    {
        ObjectWarning("Services are ALREADY stopped");
        return;
    }

    owner->setEnabled(false);

    quitting = false;

    SkVariantList p;
    p << -2;
    skApp->propagateEvent(this, "Init", "Respawn", &p);

    stopServices();
}

void Init::quit()
{
    if (!started)
    {
        ObjectWarning("Services are ALREADY stopped");
        return;
    }

    owner->setEnabled(false);

    quitting = true;

    SkVariantList p;
    p << -1;
    skApp->propagateEvent(this, "Init", "Shutdown", &p);

    stopServices();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void Init::stopServices()
{
    ObjectWarning("Stopping services ..");

    started = false;
    flowService->prepareToQuit();

    SkFlowPairsDatabase *mainDB = flowService->main();

    if (mainDB)
    {
        mainDB->setVariable("runningState", false);
        mainDB->save("database/Main.variant");
    }

    modCtrlService->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(Init, onModulesStarted)
{
    SilentSlotArgsWarning();

    if (!makeNodeConnection())
        ObjectWarning("Cannot init flow-node-connection");

    SkFlowPairsDatabase *mainDB = flowService->main();

    if (mainDB)
        mainDB->setVariable("runningState", true);

    SkVariantList p;
    p << 2;
    skApp->propagateEvent(this, "Init", "Ready", &p);
    ready();

    started = true;
}

SlotImpl(Init, onModulesStopped)
{
    SilentSlotArgsWarning();

    if (quitting)
        eventLoop()->invokeSlot(onShutdown_SLOT, this, nullptr);

    else
        start();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(Init, oneSecTick)
{
    SilentSlotArgsWarning();

    if (!isReady())
        return;
}

SlotImpl(Init, onShutdown)
{
    SilentSlotArgsWarning();
    eventLoop()->invokeSlot(owner->onGameClosed_SLOT, owner, nullptr);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool Init::makeNodeConnection()
{
    SkString fsNodeCfgFile = config->value("fsNodeCfgFile").data();

    if (fsNodeCfgFile.isEmpty())
        return true;

    SkArgsMap nodeProps;

    AssertKiller(!fsNodeCfgFile.isEmpty() && !nodeProps.fromFile(fsNodeCfgFile.c_str()));

    SkString addr = nodeProps["address"].toString();
    Short port = nodeProps["port"].toUInt16();
    SkString password = nodeProps["password"].toString();

    SkFlowPairsDatabase *mainDB = flowService->main();

    if (!mainDB)
        return false;

    return flowService->svr()->replicate(addr.c_str(), port, mainDB->getVariable("appName").data(), password.c_str());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
// Save new command transaction with (new) values

void Init::updateCommand(SkWorkerCommand *cmd)
{owner->updateCommand(cmd);}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool Init::isReady()
{
    return started;
}

SkFlowServer *Init::flowServer()
{
    return flowService->svr();
}

SkFlowPairsDatabase *Init::flowServerMainDB()
{
    return flowService->main();
}

SkModulesManager *Init::modulesManager()
{
    return modCtrlService->manager();
}

CStr *Init::superToken()
{
    return token.c_str();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
