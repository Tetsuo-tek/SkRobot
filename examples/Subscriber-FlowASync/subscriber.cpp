#include "subscriber.h"

ConstructorImpl(Subscriber, SkFlowAsync)
{
    userName = "User1";
    userPasswd = "password";
    inputChanName = "User1.Talker";
    inputChan = -1;

    setObjectName("Subscriber");

    SlotSet(init);
    SlotSet(inputData);
    SlotSet(oneSecTick);
    SlotSet(exitFromDisconnection);
    SlotSet(exitFromKernelSignal);

    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);
    Attach(this, channelDataAvailable, this, inputData, SkDirect);
    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);
}

SlotImpl(Subscriber, init)
{
    SilentSlotArgsWarning();

    if (!tcpConnect("127.0.0.1", 9000)
        || !login(userName.c_str(), userPasswd.c_str()))
    {
        skApp->quit();
        return;
    }

    Attach(this, disconnected, this, exitFromDisconnection, SkQueued);
}

void Subscriber::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (ch->name == inputChanName)
    {
        inputChan = ch->chanID;
        subscribeChannel(ch);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(Subscriber, oneSecTick)
{
    SilentSlotArgsWarning();
    checkService();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(Subscriber, inputData)
{
    SilentSlotArgsWarning();

    if (nextData())
    {
        SkFlowChannelData &d = getCurrentData();

        if (d.chanID == inputChan)
            ObjectMessage("I heard \"" << d.data.toString() << "\"");
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(Subscriber, exitFromDisconnection)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting because source is disconnected ..");
    skApp->quit();
}

SlotImpl(Subscriber, exitFromKernelSignal)
{
    SilentSlotArgsWarning();

    if (skApp->kernel_SIG->pulse_SIGNAL.getParameters()[0].toInt32() != SIGINT)
        return;

    ObjectMessage("Forcing application exit with CTRL+C");
    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

