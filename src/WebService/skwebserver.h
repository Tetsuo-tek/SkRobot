#ifndef SKWEBSERVER_H
#define SKWEBSERVER_H

#include <Modules/skabstractmodule.h>

class SkHttpService;
class SkGenericMountPoint;
class SkFsMountPoint;
class WebFrontendMountpoint;
class SkRawRedistrMountPoint;
class SkPartRedistrMountPoint;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkWebServerCfg extends SkAbstractModuleCfg
{
    public:
        Constructor(SkWebServerCfg, SkAbstractModuleCfg);

    private:
        void allowedCfgKeysSetup()              override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

struct SkWebServerFlowRedistrMP
{
    SkFlowChanID chanID;
    SkString mpPath;
    SkRawRedistrMountPoint *rawRedistr;
    SkPartRedistrMountPoint *partRedistr;
};

class SkWebServer extends SkAbstractModule
{
    SkFlowSync *sync;

    SkString svrName;
    SkHttpService *service;

    SkFsMountPoint *wwwMp;

    SkGenericMountPoint *homeAppMp;
    SkGenericMountPoint *pairsMp;
    WebFrontendMountpoint *frontendMp;
    //WebAgentMountpoint *agentMp;

    SkTreeMap<SkFlowChanID, SkWebServerFlowRedistrMP *> redistrs;

    public:
        Constructor(SkWebServer, SkAbstractModule);

        Slot(onPairsPageAccepted);
        Slot(onStreamTargetAdded);
        Slot(onStreamTargetRemoved);

    private:
        bool onSetup()                                                  override;
        void onStart()                                                  override;
        void onStop()                                                   override;

        void onSetEnabled(bool enabled)                                 override;

        void onFastTick()                                               override;
        void onSlowTick()                                               override;
        void onOneSecTick()                                             override;

        void onChannelAdded(SkFlowChanID)                               override;
        void onChannelRemoved(SkFlowChanID)                             override;
        void onChannelHeaderSetup(SkFlowChanID)                         override;

        void onChannelPublishStartRequest(SkFlowChanID chanID)          override;
        void onChannelPublishStopRequest(SkFlowChanID chanID)           override;

        void onFlowDataCome(SkFlowChannelData &chData)                  override;

        bool isInternalChannel(CStr *chanName);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // SKWEBSERVER_H
