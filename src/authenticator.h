#ifndef AUTHENTICATOR_H
#define AUTHENTICATOR_H

#include "Core/System/Network/FlowNetwork/skabstractflowauthenticator.h"

class PhysicalBody;

class Authenticator extends SkAbstractFlowAuthenticator
{
    PhysicalBody *owner;
    SkString usersDirPath;

    public:
        Constructor(Authenticator, SkAbstractFlowAuthenticator);

        void setup(CStr *usersDirectory);

        CStr *home();

    private:
        bool onAuthorization(CStr *userName, CStr *token, CStr *seed, SkFlowServicePermission *permissions)       override;

#if defined(__linux__)
        bool checkShadowPasswd(CStr *userName, CStr *token, SkFlowServicePermission *permissions);
#endif

        bool checkAuthDir(CStr *userName);
        bool authorizeAccount(CStr *userName, CStr *token, CStr *seed, SkFlowServicePermission *permissions);

        bool authorizeSysAccount(CStr *userName, CStr *token, SkFlowServicePermission *permissions);
};

#endif // AUTHENTICATOR_H
