#ifndef INIT_H
#define INIT_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "flowservice.h"
#include "modulesctrlservice.h"

#include "Core/Object/skabstractworkerobject.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#define PDB_DIR     "database"
#define TPL_DIR     "templates"
#define WWW_DIR     "www"
#define PAG_DIR     "pages.d"
#define USR_DIR     "users.d"
#define MOD_DIR     "modules.d"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class PhysicalBody;
class Config;

class SkModuleThread;
class SkAbstractModuleCfg;
class SkModulesManager;

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class Init extends SkAbstractWorkerObject
{
    public:
        Constructor(Init, SkAbstractWorkerObject);

        bool start();
        void respawn();
        void quit();

        Slot(onModulesStarted);
        Slot(onModulesStopped);

        Slot(onShutdown);
        Slot(oneSecTick);

        Signal(ready);

        bool isReady();

        SkFlowServer *flowServer();
        SkFlowPairsDatabase *flowServerMainDB();
        SkModulesManager *modulesManager();

        CStr *superToken();

    private:
        Config *config;

        SkString token;

        bool safeMode;
        bool started;
        bool quitting;//if it is true, then it will close the application when finished

        PhysicalBody *owner;
        FlowService *flowService;
        ModulesCtrlService *modCtrlService;

        bool checkRootFs();

        bool makeNodeConnection();
        void stopServices();

        void updateCommand(SkWorkerCommand *cmd)                override;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // INIT_H
