#include "robotdb.h"
#include "Core/sklogmachine.h"
#include "Core/Containers/skringbuffer.h"
#include "Core/System/Filesystem/skfsutils.h"

RobotDB::RobotDB()
{
    logger->enableOwnerName(false);
    setObjectName("PairsView");

    SkStringList acceptedKeys;
    buildCLI(acceptedKeys);
    AssertKiller(!osEnvironment->checkAllowedAppArguments(acceptedKeys));

    if (osEnvironment->existsAppArgument("--help"))
    {
        cout << osEnvironment->cmdLineHelp();
        exit(0);
    }

    /*sig.setObjectName("FlatSignal");
    FlatAttach(sig, this, cb);
    sig.trigger(&sig);
    FlatDetach(sig, cb);*/

    AssertKiller(osEnvironment->getArgsCount() < 2);
    load();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotDB::load()
{
    SkStringList l;
    osEnvironment->getAppArgumentsList(l);

    dbFilePath = osEnvironment->getAppArgument("0");
    AssertKiller(dbFilePath.isEmpty());

    SkDataBuffer fileBuff;
    AssertKiller(!SkFsUtils::readDATA(dbFilePath.c_str(), fileBuff));

    SkRingBuffer valBuff;
    valBuff.addData(fileBuff.data(), fileBuff.size());

    db.fromData(&valBuff);
    AssertKiller(db.isNull());

    if (osEnvironment->existsAppArgument("--show-db-content")
        || osEnvironment->existsAppArgument("--show-keys")
        || osEnvironment->existsAppArgument("--show-var-content"))
    {
        db.copyToMap(dbMap);
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

int RobotDB::exec()
{
    cout << "\n";

    if (osEnvironment->existsAppArgument("--show-db-content"))
    {
        SkString json;
        db.toJson(json, osEnvironment->existsAppArgument("--human-readable"));
        FlatMessage("JSON content of database: '" << dbFilePath << "'\n -> " << json);
    }

    else if (osEnvironment->existsAppArgument("--show-keys"))
    {
        SkBinaryTreeVisit<SkString, SkVariant> *itr = dbMap.iterator();

        while(itr->next())
        {
            SkString &k = itr->item().key();
            SkVariant &v = itr->item().value();

            FlatMessage(" - " << k << " [" << v.variantTypeName() << "]");
        }

        delete itr;

        cout << "\n";
        FlatMessage("There are " << dbMap.count() << " root-variables inside database");
    }

    else if (osEnvironment->existsAppArgument("--show-var-content"))
    {
        SkString &key = osEnvironment->getAppArgument("--show-var-content");
        AssertKiller(!dbMap.contains(key));
        SkString json;
        SkVariant &v = dbMap[key];
        v.toJson(json, osEnvironment->existsAppArgument("--human-readable"));
        FlatMessage("JSON content of variable: '" << key << "' [" << v.variantTypeName() << "]\n -> " << json);
    }

    cout << "\n";
    return 0;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotDB::buildCLI(SkStringList &acceptedKeys)
{
    acceptedKeys.append("--help");

    acceptedKeys.append("0");
    acceptedKeys.append("--show-db-content");
    acceptedKeys.append("--show-keys");
    acceptedKeys.append("--show-var-content");
    acceptedKeys.append("--human-readable");

    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

    osEnvironment->addCliAppHelp("--help", "prints this output and exit");

    osEnvironment->addCliAppHelp("0", "set the database filePath");
    osEnvironment->addCliAppHelp("--show-db-content", "prints database content");
    osEnvironment->addCliAppHelp("--show-keys", "prints database variable-keys list");
    osEnvironment->addCliAppHelp("--show-var-content <KEY_NAME>", "prints variable content");
    osEnvironment->addCliAppHelp("--human-readable", "prints contents as human-readable JSON");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

/*void RobotDB::cb(SkFlatObject *_this, SkFlatObject *referer)
{
    cout << "!!!!!!!!!!!!!!!! " << _this->typeName() << " " << dynamic_cast<SkFlatSignal *>(referer)->objectName() << "\n";
}*/
