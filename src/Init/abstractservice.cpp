﻿#include "abstractservice.h"
#include "../physicalbody.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

AbstractConstructorImpl(AbstractService, SkObject)
{
    owner = nullptr;
    cfg = nullptr;

    running = false;

    SignalSet(enabled);
    SignalSet(disabled);
    SignalSet(errorOccurred);

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);

    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);
}

bool AbstractService::init(PhysicalBody *workerOwner, Config *config)
{
    if (isRunning())
    {
        ObjectWarning("Service is ALREADY running!");
        return false;
    }

    ObjectWarning("Service is initializing ..");

    owner = workerOwner;
    cfg = config;

    bool ok = onInit();

    if (ok)
    {
        running = true;
        ObjectMessage("Service is UP");

        //MUST BE DERIVATING CLASS TO TRIGGER THIS SIG; it could be asynchronous
        //enabled();
    }

    else
    {
        running = false;
        ObjectError("Service problem");
        errorOccurred();
    }

    return ok;
}

void AbstractService::quit()
{
    if (!isRunning())
    {
        ObjectWarning("Service is NOT running!");
        return;
    }

    ObjectWarning("Service is quiting ..");
    onQuit();

    ObjectMessage("Service is DOWN");

    //MUST BE DERIVATING CLASS TO TRIGGER THIS SIG; it could be asynchronous
    //disabled();
    running = false;

    owner = nullptr;
    cfg = nullptr;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(AbstractService, fastTick)
{
    SilentSlotArgsWarning();

    if (!isRunning())
        return;

    onFastTick();
}

SlotImpl(AbstractService, slowTick)
{
    SilentSlotArgsWarning();

    if (!isRunning())
        return;

    onSlowTick();
}

SlotImpl(AbstractService, oneSecTick)
{
    SilentSlotArgsWarning();

    if (!isRunning())
        return;

    onOneSecTick();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool AbstractService::isRunning()
{
    return running;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
