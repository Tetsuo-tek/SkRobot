#include "robotmonitor.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    logger->enable(false);

    skApp->init(100000, 250000, SK_TIMEDLOOP_SLEEPING);

    RobotMonitor *a = new RobotMonitor;
    Attach(skApp->started_SIG, pulse, a, init, SkOneShotDirect);

    return skApp->exec();
}
