#ifndef GUI_H
#define GUI_H

const unsigned gui_count = 1;

const struct {
	const bool isDir;
	const char *relativePath;
	const char *name;
	const unsigned size;
	const char *data;
} gui[1] = {
	{
		false,
		"gui/",
		"mainwindow.json",
		2704,
		"{\n" \
		"    \"name\" : \"window\",\n" \
		"    \"type\" : \"DoubleWindow\",\n" \
		"    \"title\" : \"Sensor meter\",\n" \
		"    \"region\" : [0, 0, 800, 480],\n" \
		"    \"color\" : [80, 80, 80],\n" \
		"    \"children\" : [\n" \
		"        {\n" \
		"            \"name\" : \"testToggleButton\",\n" \
		"            \"type\" : \"LightButton\",\n" \
		"            \"region\" : [0, 0, 100, 25],\n" \
		"            \"label\" : \"Toggle\",\n" \
		"            \"align\" : \"ALIGN_CENTER\"\n" \
		"        },\n" \
		"        {\n" \
		"            \"name\" : \"testPulseButton\",\n" \
		"            \"type\" : \"Button\",\n" \
		"            \"region\" : [0, 30, 100, 25],\n" \
		"            \"label\" : \"Pulse\",\n" \
		"            \"align\" : \"ALIGN_CENTER\"\n" \
		"        },\n" \
		"        {\n" \
		"            \"name\" : \"led\",\n" \
		"            \"type\" : \"Led\",\n" \
		"            \"region\" : [120, 0, 100, 50],\n" \
		"            \"onColor\" : [0, 0, 255],\n" \
		"            \"label\" : \"PRESSED\"\n" \
		"        },\n" \
		"        {\n" \
		"            \"name\" : \"lcdDisplay\",\n" \
		"            \"type\" : \"LcdDisplay\",\n" \
		"            \"region\" : [120, 60, 200, 50],\n" \
		"            \"segmentsColor\" : [0, 0, 255],\n" \
		"            \"fixed\" : 4,\n" \
		"            \"precision\" : 1,\n" \
		"            \"resizable\" : true\n" \
		"        },\n" \
		"        {\n" \
		"            \"name\" : \"gauge\",\n" \
		"            \"type\" : \"Gauge\",\n" \
		"            \"box\" : \"DOWN_BOX\",\n" \
		"            \"bounds\" : [0.0, 100.0],\n" \
		"            \"region\" : [120, 120, 200, 200]\n" \
		"        },\n" \
		"        {\n" \
		"            \"name\" : \"buttonsPack\",\n" \
		"            \"type\" : \"Pack\",\n" \
		"            \"orientation\" : \"HORIZONTAL\",\n" \
		"            \"spacing\" : 3,\n" \
		"            \"region\" : [2, 453, 800, 25],\n" \
		"            \"children\" : [\n" \
		"                {\n" \
		"                    \"name\" : \"fsButton\",\n" \
		"                    \"type\" : \"LightButton\",\n" \
		"                    \"size\" : [100, 0],\n" \
		"                    \"label\" : \"Fullscreen\",\n" \
		"                    \"tooltip\" : \"Toggle fullscreen\",\n" \
		"                    \"align\" : \"ALIGN_CENTER\"\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"spacer\",\n" \
		"                    \"type\" : \"Box\",\n" \
		"                    \"size\" : [594, 0],\n" \
		"                    \"resizable\" : true\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"rx\",\n" \
		"                    \"type\" : \"Led\",\n" \
		"                    \"size\" : [10, 0],\n" \
		"                    \"timeoutMS\" : 150,\n" \
		"                    \"onColor\" : [0, 255, 0]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"tx\",\n" \
		"                    \"type\" : \"Led\",\n" \
		"                    \"size\" : [10, 0],\n" \
		"                    \"timeoutMS\" : 150,\n" \
		"                    \"onColor\" : [255, 0, 0]\n" \
		"                },\n" \
		"                {\n" \
		"                    \"name\" : \"quitButton\",\n" \
		"                    \"type\" : \"Button\",\n" \
		"                    \"size\" : [70, 0],\n" \
		"                    \"label\" : \"Quit\",\n" \
		"                    \"tooltip\" : \"Close application\",\n" \
		"                    \"align\" : \"ALIGN_CENTER\"\n" \
		"                }\n" \
		"            ]\n" \
		"        }\n" \
		"    ]\n" \
		"}\n"
	}
};

#endif // GUI_H
