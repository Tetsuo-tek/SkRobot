#include "audiocap.h"


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/Containers/skarraycast.h>

ConstructorImpl(AudioCap, SkFlowSat)
{
    pa = nullptr;
    input = nullptr;
    outputChanName = "PCM.";
    outputChanID = -1;
    outputChan = nullptr;
    data_t = T_NULL;

    setObjectName("AudioCap");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool AudioCap::onSetup()
{
    SkCli *cli = skApp->appCli();

    device  = cli->value("--device").toString();
    uint channels = cli->value("--channels").toUInt();
    uint sampleRate = cli->value("--sample-rate").toUInt();
    uint bufferFrames = cli->value("--buffer-frames").toUInt();
    SkString fmt = cli->value("--format").toString();

    SkAudioFmtType fmt_t;

    if (fmt == "UCHAR")
    {
        fmt_t = AFMT_UCHAR;
        data_t = T_INT8;
        outputChanProps["min"] = 0;
        outputChanProps["max"] = 255;
        outputChanName.append("UChar");
    }

    else if (fmt == "SHORT")
    {
        fmt_t = AFMT_SHORT;
        data_t = T_INT16;
        outputChanProps["min"] = -32768;
        outputChanProps["max"] = 32767;
        outputChanName.append("Short");
    }

    else if (fmt == "INT")
    {
        fmt_t = AFMT_INT;
        data_t = T_INT32;
        outputChanProps["min"] = -2147483648;
        outputChanProps["max"] = 2147483647;
        outputChanName.append("Int");
    }

    else if (fmt == "FLOAT")
    {
        fmt_t = AFMT_FLOAT;
        data_t = T_FLOAT;
        outputChanProps["min"] = -1.f;
        outputChanProps["max"] = 1.f;
        outputChanName.append("Float");
    }

    else
    {
        ObjectError("Format is NOT valid [Allowed: UCHAR, SHORT, INT, FLOAT]: " << fmt);
        return false;
    }

    if (!params.set(false, channels, sampleRate, bufferFrames, fmt_t))
        return false;

    params.toMap(outputChanProps);

    outputChanProps["stream"] = "PCM";
    outputChanProps["codec"] = "RAW";

    setLogin("guest", "password");

    return true;
}

void AudioCap::onInit()
{
    ulong tickIntervalUS = static_cast<ulong>(params.getTickTimePeriod() * 1000000.);

    eventLoop()->changeFastZone(tickIntervalUS);
    eventLoop()->changeSlowZone(tickIntervalUS*4);

    addStreamingChannel(outputChanID, FT_AUDIO_DATA, data_t, outputChanName.c_str(), "audio/wav", &outputChanProps);
}

void AudioCap::onQuit()
{
    stop();
}

void AudioCap::onChannelAdded(SkFlowChanID chanID)
{
    if (outputChan)
        return;

    if (outputChanID == chanID)
    {
        outputChan = channel(chanID);

        if (!rec())
            skApp->quit();
    }
}

void AudioCap::onFastTick()
{
    if (!input || !input->isCanonicalSizeReached() || !outputChan)
        return;

    if (!outputChan->isPublishingEnabled)
    {
        if (!input->isEmpty())
            input->clear();

        return;
    }

    ULong sz = input->getCanonicalSize();
    char *data = new char [sz];
    input->getCanonicalBuffer(data);
    publish(outputChan->chanID, data, sz);
    delete [] data;
}

bool AudioCap::rec()
{
    pa = new SkPortAudio(this);
    pa->setObjectName(this, "PortAudio");

    if (!pa->init())
    {
        pa->destroyLater();
        pa = nullptr;
        return false;
    }

    input = pa->getProduction().inputProduction;

    if (!pa->rec(device.c_str(), params))
    {
        pa->destroyLater();
        pa = nullptr;
        input = nullptr;
        return false;
    }

    input->setCanonicalSize(params.getCanonicalSize());
    ObjectMessage("Recording STARTED [device: " << device << "]");

    return true;
}

void AudioCap::stop()
{
    if (!pa)
        return;

    if (pa->isInitialized())
    {
        if (pa->isStreamActive())
            pa->stop();

        pa->close();
    }

    pa->destroyLater();
    pa = nullptr;

    input = nullptr;

    ObjectMessage("Recording STOPPED");
}
