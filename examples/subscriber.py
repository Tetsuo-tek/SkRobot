#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow import FlowChannel
from PySketch.flowproto import Variant_T, Flow_T
from PySketch.flowsat import FlowSat

###############################################################################
# SKETCH

userName = "User1"
userPasswd = "password"
inputChanName = "User1.PyTalker"

inputChan = None

sat = FlowSat()

def setup():
    print("[SETUP] ..")

    sat.setLogin(userName, userPasswd)

    t = 0.005 # seconds
    sat.setTickTimer(t, t * 50)

    sat.setNewChanCallBack(onChannelAdded)
    sat.setGrabDataCallBack(onDataGrabbed)

    ok = sat.connect() # uses the env-var ROBOT_ADDRESS

    if ok:
        sat.setSpeedMonitorEnabled(False)
        print("[LOOP] ..")
    
    return ok

def loop():
    sat.tick()
    return sat.isConnected()

###############################################################################
# CALLBACKs

def onChannelAdded(ch):
    global inputChan

    if ch.name == inputChanName:
        inputChan = ch
        sat.subscribeChannel(inputChan.chanID)

def onDataGrabbed(chanID, data):
    if inputChan is None:
        return
    
    if inputChan.chanID == chanID:
        txt = data.decode('utf-8')
        print("I heard \"{}\"".format(txt))
    
###############################################################################
