#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.flowsat       import FlowSat
from PySketch.loop          import LoopTimerMode
from PySketch.log           import escapes, msg, dbg, wrn, err, cri, raw, printPair
from PySketch.blobchan      import BlobService, BlobUploader, BlobDownloader
from PySketch.fs            import *
from PySketch.term          import *

###############################################################################

import os
import sys
import re
import signal
import readline
import glob
import argparse

from queue                  import Queue

###############################################################################
# SKETCH

args = None

sat = FlowSat()
blobService = None

cmdPack = None
cmdQueue = Queue()
uploader = None
downloader = None

def setup():
    global args
    global blobService

    wrn("[SETUP] ..")

    parser = argparse.ArgumentParser(description="Robot LLM prompt")
    parser.add_argument("sketchfile", help="Sketch program file")
    parser.add_argument("--user", help="Flow-network username", default="guest")
    parser.add_argument("--password", help="Flow-network password", default="password")

    args = parser.parse_args()
    
    sat.setTickTimer(0.025, 0.100, 0.210, LoopTimerMode.TIMEDLOOP_RT)
    sat.setLogin(args.user, args.password)

    ok = sat.connect()

    if ok:
        sat.setSpeedMonitorEnabled(False)

        blobService = BlobService(sat)
        blobService.responsed.attach(onBlobStoreResponse)
        ok = blobService.create("BlobStore")

        wrn("[LOOP] ..")
    
    return ok

def loop():
    if cmdPack is None and not cmdQueue.empty():
        checkConsoleCommand()

    sat.tick()
    return sat.isConnected()

###############################################################################

def close():
    os.kill(os.getpid(), signal.SIGINT)

def signalHandler(signal, frame):
    cri("Signal received, exiting ..")
    raise KeyboardInterrupt

###############################################################################
# COMMANDs-QUEUE

def checkConsoleCommand():
    global cmdPack
    global cmdQueue
    global uploader
    global downloader

    cmdPack = cmdQueue.get()
    
    cmd = cmdPack["cmd"].upper()
    args = cmdPack["args"]
    paths = cmdPack["paths"]

    if cmd == "PUT":
        if len(args) < 1 or len(paths) < 1:
            err("The upload commands REQUIRE file-path and, optionally, " \
                "the remote target parent-path, that is $TEMP by default")

            cmdPack = None
            return

        targetParentPath = "$TEMP"

        if len(args) > 1:
            targetParentPath=args[1]

        uploader = BlobUploader(blobService)

        if not uploader.start(paths[0], targetParentPath):
            uploader = None
            cmdPack = None

    elif cmd == "GET":
        if len(args) < 1:
            err("The download command REQUIRE file-path and, optionally, the local target parent-path")
            cmdPack = None
            return

        localParentPath=""

        if len(args) == 2:
            localParentPath=args[1]

        downloader = BlobDownloader(blobService)

        if not downloader.start(args[0], localParentPath):
            downloader = None
            cmdPack = None

    else:
        blobService.sendCommand(cmd, args)

###############################################################################
# CALLBACKs

def onBlobStoreResponse(status, message, data):
    global cmdPack

    if data is not None:
        raw("{}{}{}\n".format(escapes["WHT"], data, escapes["RESET"]))

    if len(message) > 0:
        if not status:
            err(message)
        
        else:
            msg(message)
    
    elif not status:
        err("ERROR")

    cmdPack = None

###############################################################################
# SHELL

def completePath(text, state):    
    text = os.path.expanduser(text)

    if not text:
        options = glob.glob("./*")
    else:
        options = glob.glob(text + "*")
    
    options = sorted(options)
        
    if state < len(options):
        return options[state]
    
    return None

def displayMatches(substitution, matches, longest_match_length):
    print("\n")

    for match in matches:
        print(match)

    print("\n> {}".format(readline.get_line_buffer()), end="", flush=True)

def findPaths(l):
    potentialPaths = re.findall(r"([~/]?[\w\./~-]+)", l)
    expandedPaths = [os.path.expanduser(path) for path in potentialPaths]
    return [path for path in expandedPaths if pathExists(path) and (isFile(path) or isDir(path)) ]

def main():
    signal.signal(signal.SIGINT, signalHandler)

    readline.set_completer(completePath)
    readline.set_completer_delims(" \t\n;")
    readline.parse_and_bind("tab: complete")
    readline.set_completion_display_matches_hook(displayMatches)

    while True:
        try:
            prompt = input().strip()

            if prompt is None:
                break

            if len(prompt) == 0:
                #print("> ", end="", flush=True)
                break

            if prompt.lower() in ["exit", "bye", "quit"]:
                wrn("Closing ..")
                break

            paths = findPaths(prompt)
            #print("Prompt file paths:", paths)

            if len(prompt) == 0 and len(paths) == 0:
                msg("Exiting ..")
                break
            
            args = prompt.split(" ")
            cmd = args[0]
            
            if len(args) > 1:
                args = args[1:]

            else:
                args = []

            tempPromptPack = {
                "cmd" : cmd,
                "args": args,
                "paths" : paths
            }

            cmdQueue.put(tempPromptPack)

        except EOFError:
            break
            
        except KeyboardInterrupt:
            break

###############################################################################