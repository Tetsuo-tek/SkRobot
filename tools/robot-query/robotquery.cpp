﻿#include "robotquery.h"
#include <Core/System/skosenv.h>
#include "Core/Containers/skringbuffer.h"
#include "Core/System/Filesystem/skfsutils.h"
#include "Core/System/IPC/skstdinput.h"

#include <termios.h>

ConstructorImpl(RobotQuery, SkFlowSync)
{
    setObjectName("RobotQuery");

    SlotSet(init);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotQuery, init)
{
    SilentSlotArgsWarning();

    SkStringList acceptedKeys;
    buildCLI(acceptedKeys);
    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));

    if (osEnv()->existsAppArgument("--help"))
    {
        cout << osEnv()->cmdLineHelp();
        skApp->quit();
        return;
    }

    logger->enableOwnerName(false);
    logger->enable(true);

    buildSync();

    execute();
    logger->enable(false);

    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotQuery::buildSync()
{
    AssertKiller(!osEnv()->existsEnvironmentVar("ROBOT_ADDRESS"));
    SkString robotAddress = osEnv()->getEnvironmentVar("ROBOT_ADDRESS");

    if (robotAddress.startsWith("local:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address [$ROBOT_ADDRESS]: " << robotAddress);

        AssertKiller(!localConnect(robotAddress.c_str()));
    }

    else if (robotAddress.startsWith("tcp:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address [$ROBOT_ADDRESS]: " << robotAddress);

        SkStringList robotAddressParsed;
        robotAddress.split(":", robotAddressParsed);
        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);

        if (robotAddressParsed.count() == 1)
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));

        else
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));
    }

    SkString userName = "User1";
    SkString userPasswd = "password";

    /*cout << "\nRobot login:\nUser: ";
    cin >> userName;

    cout << "Password: ";
    SkStdInput::unSetTermAttr(ECHO);
    cin >> userPasswd;
    SkStdInput::setTermAttr(ECHO);*/

    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotQuery::execute()
{
    cout << "\n";

    if (osEnv()->existsAppArgument("--robot-name"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));
        getVariable("appName", v);

        FlatMessage("Robot name: " << v.toString() << "\n");
    }

    else if (osEnv()->existsAppArgument("--robot-config"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));
        getVariable("Application_CFG", v);

        SkTreeMap<SkString, SkVariant> mods;
        v.copyToMap(mods);

        SkBinaryTreeVisit<SkString, SkVariant> *itr = mods.iterator();

        while(itr->next())
        {
            SkString &k = itr->item().key();
            SkVariant &v = itr->item().value();

            FlatMessage(" - " << k << " [Value: " << v << "]");
        }

        delete itr;

        cout << "\n";
        FlatMessage("There are " << mods.count() << " configuration parameters on the Robot\n");
    }

    else if (osEnv()->existsAppArgument("--robot-trans"))
    {
        AssertKiller(!setCurrentDbName("Main"));

        SkStringList keys;
        variablesKeys(keys);

        int c = 0;

        for(ULong i=0; i<keys.count(); i++)
            if (keys[i].endsWith("_TRANS"))
            {
                FlatMessage(" - " << keys[i]);
                c++;
            }

        cout << "\n";
        FlatMessage("There are " << c << " command/control Transactions on the Robot core\n");
    }

    else if (osEnv()->existsAppArgument("--robot-tick"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));

        FlatMessage("Tick parameters for Robot\n");

        getVariable("fastTickTime", v);
        FlatMessage(" - Fast zone average: " << v << " μs");

        getVariable("slowTickTime", v);
        FlatMessage(" - Slow zone average: " << v << " μs");

        getVariable("tickMode", v);
        FlatMessage(" - Tick mode: " << SkEventLoop::getTimerModeName(static_cast<SkLoopTimerMode>(v.toInt())));

        getVariable("jobLoadAvg", v);
        FlatMessage(" - Job load: " << v << " %\n");
    }

    else if (osEnv()->existsAppArgument("--robot-ticks"))
    {
        float minimumTick = 0.f;
        float maximumLoad = 0.f;
        float tickAvgTime = 0.f;
        float consumedAvgTime = 0.f;
        float avgLoad = 0.f;

        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));

        getVariable("tickAvg", v);
        tickAvgTime = v.toFloat();
        minimumTick = tickAvgTime;

        getVariable("consumedAvg", v);
        consumedAvgTime = v.toFloat();

        avgLoad = consumedAvgTime/tickAvgTime;
        maximumLoad = avgLoad;

        FlatMessage(" - Core [load: " << avgLoad << " %; Tick: " << tickAvgTime << " ms; Consumed: " << consumedAvgTime << " ms]");

        AssertKiller(!setCurrentDbName("Main"));
        getVariable("modules", v);

        SkTreeMap<SkString, SkVariant> mods;
        v.copyToMap(mods);
        SkStringList modNames;
        mods.keys(modNames);

        for(ULong i=0; i<modNames.count(); i++)
        {
            setCurrentDbName(modNames[i].c_str());

            getVariable("tickAvg", v);
            tickAvgTime = v.toFloat();

            if (tickAvgTime < minimumTick)
                minimumTick = tickAvgTime;

            if (avgLoad > maximumLoad)
                maximumLoad = avgLoad;

            getVariable("consumedAvg", v);
            consumedAvgTime = v.toFloat();

            avgLoad = consumedAvgTime/tickAvgTime;

            if (avgLoad>=0.8)
                FlatWarning(" - " << modNames[i] << " [Load: " << SkString(avgLoad, 3, true) << " %; Tick: " << tickAvgTime << " ms; Consumed: " << consumedAvgTime << " ms]");
            else
                FlatMessage(" - " << modNames[i] << " [Load: " << SkString(avgLoad, 3, true) << " %; Tick: " << tickAvgTime << " ms; Consumed: " << consumedAvgTime << " ms]");
        }

        cout << "\n";
        FlatMessage("Minimum tick-interval is " << minimumTick << " ms, Maximum load is: " << SkString(maximumLoad, 3, true) << " %\n");
    }

    else if (osEnv()->existsAppArgument("--robot-os-info"))
    {
        FlatMessage("Operating system:\n");

        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));

        getVariable("hostName", v);
        FlatMessage(" - HostName: " << v);

        getVariable("platform", v);
        FlatMessage(" - Platform: " << v);

        getVariable("osName", v);
        FlatMessage(" - Name: " << v);

        getVariable("osRelease", v);
        FlatMessage(" - Release: " << v);

        getVariable("osVersion", v);
        FlatMessage(" - Version: " << v);

        cout << "\n";
        FlatMessage("Processes:\n");

        getVariable("processesRunning", v);
        FlatMessage(" - Running: " << v);

        getVariable("processesSleeping", v);
        FlatMessage(" - Sleeping: " << v);

        getVariable("processesStopped", v);
        FlatMessage(" - Stopped: " << v);

        getVariable("processesZombie", v);
        FlatMessage(" - Zombies: " << v);

        getVariable("processesTotal", v);
        FlatMessage(" - Total: " << v);

        cout << "\n";
    }

    else if (osEnv()->existsAppArgument("--robot-sys-info"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));

        FlatMessage("CPU:\n");

        getVariable("processors", v);
        FlatMessage(" - Core number: " << v);

        getVariable("bitsWidth", v);
        FlatMessage(" - BitsWidth: " << v);

        getVariable("userTime", v);
        FlatMessage(" - User: " << v << " %");

        getVariable("kernelTime", v);
        FlatMessage(" - Kernel: " << v << " %");

        getVariable("ioTime", v);
        FlatMessage(" - IO: " << v << " %");

        getVariable("swapTime", v);
        FlatMessage(" - Swap: " << v << " %");

        getVariable("niceTime", v);
        FlatMessage(" - Nice: " << v << " %");

        getVariable("idleTime", v);
        FlatMessage(" - Idle: " << v << " %");

        getVariable("load1", v);
        FlatMessage(" - Load (1): " << v);

        getVariable("load5", v);
        FlatMessage(" - Load (5): " << v);

        getVariable("load15", v);
        FlatMessage(" - Load (15): " << v);

        cout << "\n";

        getVariable("ramUsedPercent", v);
        FlatMessage("RAM (" << v << " %):\n");

        getVariable("ramTotal", v);
        FlatMessage(" - Total: " << v << " GB");

        getVariable("ramUsed", v);
        FlatMessage(" - Used: " << v << " GB");

        getVariable("ramCache", v);
        FlatMessage(" - Cached: " << v << " GB");

        getVariable("ramFree", v);
        FlatMessage(" - Free: " << v << " GB");

        getVariable("pagesIn", v);
        FlatMessage(" - Pages in: " << v);

        getVariable("pagesOut", v);
        FlatMessage(" - Pages out: " << v);

        cout << "\n";

        getVariable("swapUsedPercent", v);
        FlatMessage("SWAP (" << v << " %)\n");

        getVariable("swapTotal", v);
        FlatMessage(" - Total: " << v << " GB");

        getVariable("swapUsed", v);
        FlatMessage(" - Used: " << v << " GB");

        getVariable("swapFree", v);
        FlatMessage(" - Free: " << v << " GB");

        cout << "\n";
    }

    else if (osEnv()->existsAppArgument("--robot-storage-info"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));

        SkStringList storages;
        SkStringList storages_tx;
        SkStringList storages_rx;

        getVariable("storages", v);
        v.copyToStringList(storages);

        getVariable("storages_tx", v);
        v.copyToStringList(storages_tx);

        getVariable("storages_rx", v);
        v.copyToStringList(storages_rx);

        for(ULong i=0; i<storages.count(); i++)
            FlatMessage(" - " << storages[i] << " [Tx: " << storages_tx[i] << " MB/s; Rx: " << storages_rx[i] << " MB/s]");

        cout << "\n";
        FlatMessage("There are " << storages.count() << " storage devices on the Robot\n");
    }

    else if (osEnv()->existsAppArgument("--robot-network-info"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));

        SkStringList networks;
        SkStringList networks_tx;
        SkStringList networks_rx;

        getVariable("networks", v);
        v.copyToStringList(networks);

        getVariable("networks_tx", v);
        v.copyToStringList(networks_tx);

        getVariable("networks_rx", v);
        v.copyToStringList(networks_rx);

        for(ULong i=0; i<networks.count(); i++)
            FlatMessage(" - " << networks[i] << " [Tx: " << networks_tx[i] << " MB/s; Rx: " << networks_rx[i] << " MB/s]");

        cout << "\n";
        FlatMessage("There are " << networks.count() << " network interfaces on the Robot\n");
    }

    else if (osEnv()->existsAppArgument("--robot-uptime"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));
        getVariable("upTime", v);

        FlatMessage("Robot upTime: " << v.toString() << "\n");
    }

    else if (osEnv()->existsAppArgument("--ls-modules"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));
        getVariable("modules", v);

        SkTreeMap<SkString, SkVariant> mods;
        v.copyToMap(mods);

        SkBinaryTreeVisit<SkString, SkVariant> *itr = mods.iterator();

        while(itr->next())
        {
            SkString &k = itr->item().key();
            SkVariant &v = itr->item().value();

            FlatMessage(" - " << k << " [Class: " << v << "]");
        }

        delete itr;

        cout << "\n";
        FlatMessage("There are " << mods.count() << " running modules on the Robot\n");
    }

    else if (osEnv()->existsAppArgument("--sat-config"))
    {
        SkVariant v;
        SkString satName = osEnv()->getAppArgument("--sat-config");
        AssertKiller(!existsOptionalPairDb(satName.c_str()));
        AssertKiller(!setCurrentDbName(satName.c_str()));

        SkString key = satName;
        key.append("_CFG");
        getVariable(key.c_str(), v);

        SkString json;
        v.toJson(json, osEnv()->existsAppArgument("--human-readable"));
        FlatMessage("JSON content of configuration for satellite: '" << satName << "'\n" << json << "\n");
    }

    else if (osEnv()->existsAppArgument("--sat-trans"))
    {
        SkString satName = osEnv()->getAppArgument("--sat-trans");

        AssertKiller(satName=="Main");
        AssertKiller(!existsOptionalPairDb(satName.c_str()));
        AssertKiller(!setCurrentDbName(satName.c_str()));

        SkStringList keys;
        variablesKeys(keys);

        int c = 0;

        for(ULong i=0; i<keys.count(); i++)
            if (keys[i].endsWith("_TRANS"))
            {
                FlatMessage(" - " << keys[i]);
                c++;
            }

        cout << "\n";
        FlatMessage("There are " << c << " command/control Transactions on satellite: '" << satName << "'\n");
    }

    else if (osEnv()->existsAppArgument("--sat-tick"))
    {
        SkVariant v;
        SkString satName = osEnv()->getAppArgument("--sat-tick");
        AssertKiller(!existsOptionalPairDb(satName.c_str()));
        AssertKiller(!setCurrentDbName(satName.c_str()));

        FlatMessage("Tick parameters for satellite: " << satName << "\n");

        getVariable("fastTickTime", v);
        FlatMessage(" - Fast zone average: " << v << " μs");

        getVariable("slowTickTime", v);
        FlatMessage(" - Slow zone average: " << v << " μs");

        getVariable("tickMode", v);
        FlatMessage(" - Tick mode: " << SkEventLoop::getTimerModeName(static_cast<SkLoopTimerMode>(v.toInt())));

        getVariable("jobLoadAvg", v);
        FlatMessage(" - Job load: " << v << " %\n");
    }

    else if (osEnv()->existsAppArgument("--ls-databases"))
    {
        SkVariant v;
        AssertKiller(!setCurrentDbName("Main"));
        getVariable("databases", v);

        SkStringList dbList;
        v.copyToStringList(dbList);

        for(ULong i=0; i<dbList.count(); i++)
            FlatMessage(" - " << dbList[i]);

        cout << "\n";
        FlatMessage("There are " << dbList.count() << " active database on the Robot\n");
    }

    else if (osEnv()->existsAppArgument("--ls-db-keys"))
    {
        SkString dbName = osEnv()->getAppArgument("--ls-db-keys");
        AssertKiller(!existsOptionalPairDb(dbName.c_str()));
        AssertKiller(!setCurrentDbName(dbName.c_str()));

        SkStringList keys;
        variablesKeys(keys);

        for(ULong i=0; i<keys.count(); i++)
            FlatMessage(" - " << keys[i]);

        cout << "\n";
        FlatMessage("There are " << keys.count() << " keys on selected database: " << osEnv()->getAppArgument("--set-current-db") << "\n");
    }

    if (osEnv()->existsAppArgument("--show-db-content"))
    {
        SkString dbName = osEnv()->getAppArgument("--show-db-content");
        AssertKiller(dbName.isEmpty());

        SkArgsMap db;
        AssertKiller(!setCurrentDbName(dbName.c_str()));
        getAllVariables(db);

        SkString json;
        db.toString(json, osEnv()->existsAppArgument("--human-readable"));
        FlatMessage("JSON content of database: '" << dbName << "'\n" << json << "\n");
    }

    else if (osEnv()->existsAppArgument("--show-var-content"))
    {
        SkString &dbkeyName = osEnv()->getAppArgument("--show-var-content");

        SkStringList keyParsed;
        dbkeyName.split(".", keyParsed);
        AssertKiller(keyParsed.count() != 2);

        SkString &dbName = keyParsed.first();
        SkString key = keyParsed.last();
        AssertKiller(dbName.isEmpty() || dbkeyName.isEmpty());

        SkVariant v;
        AssertKiller(!setCurrentDbName(dbName.c_str()));
        getVariable(key.c_str(), v);

        SkString json;
        v.toJson(json, osEnv()->existsAppArgument("--human-readable"));
        FlatMessage("JSON content of variable: '" << dbkeyName << "' [" << v.variantTypeName() << "]\n" << json << "\n");
    }

    else if (osEnv()->existsAppArgument("--ls-stream-channels"))
    {
        updateChannels();

        int c = 0;
        SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *itr = channelsIterator();

        while(itr->next())
        {
            SkFlowChannel *ch = itr->item().value();

            if (ch->chan_t == StreamingChannel)
            {
                FlatMessage(" - " << ch->name << " [ChanID: " << ch->chanID << "; Flow_T: " << SkFlowProto::flowTypeToString(ch->flow_t) << "; Data_T: " << SkVariant::variantTypeName(ch->data_t) << "; Mime_T: " << ch->mime << "]");
                c++;
            }
        }

        delete itr;

        cout << "\n";
        FlatMessage("There are " << c << " StreamingChannels on the Robot\n");
    }

    else if (osEnv()->existsAppArgument("--ls-service-channels"))
    {
        updateChannels();

        int c = 0;
        SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *itr = channelsIterator();

        while(itr->next())
        {
            SkFlowChannel *ch = itr->item().value();

            if (ch->chan_t == ServiceChannel)
            {
                FlatMessage(" - " << ch->name << " [ChanID: " << ch->chanID << "]");
                c++;
            }
        }

        delete itr;

        cout << "\n";
        FlatMessage("There are " << c << " ServiceChannels on the Robot\n");
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotQuery::buildCLI(SkStringList &acceptedKeys)
{
    acceptedKeys.append("--help");

    acceptedKeys.append("--robot-name");
    acceptedKeys.append("--robot-config");
    acceptedKeys.append("--robot-trans");
    acceptedKeys.append("--robot-tick");
    acceptedKeys.append("--robot-ticks");
    acceptedKeys.append("--robot-os-info");
    acceptedKeys.append("--robot-sys-info");
    acceptedKeys.append("--robot-storage-info");
    acceptedKeys.append("--robot-network-info");
    acceptedKeys.append("--robot-uptime");

    acceptedKeys.append("--ls-modules");

    acceptedKeys.append("--sat-config");
    acceptedKeys.append("--sat-trans");
    acceptedKeys.append("--sat-tick");

    acceptedKeys.append("--ls-databases");
    acceptedKeys.append("--ls-db-keys");
    acceptedKeys.append("--show-db-content");
    acceptedKeys.append("--show-var-content");
    acceptedKeys.append("--human-readable");

    acceptedKeys.append("--ls-stream-channels");
    acceptedKeys.append("--ls-service-channels");


    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

    osEnv()->addCliAppHelp("--help", "prints this output and exit");

    osEnv()->addCliAppHelp("--robot-name", "shows robot name");
    osEnv()->addCliAppHelp("--robot-config", "shows robot running configuration");
    osEnv()->addCliAppHelp("--robot-trans", "shows robot command/control Transactions");
    osEnv()->addCliAppHelp("--robot-tick", "shows robot core tick parameters");
    osEnv()->addCliAppHelp("--robot-ticks", "shows robot ticks and loads");
    osEnv()->addCliAppHelp("--robot-os-info", "shows robot operating-system informations");
    osEnv()->addCliAppHelp("--robot-sys-info", "shows robot pshysical-system informations");
    osEnv()->addCliAppHelp("--robot-storage-info", "shows robot storage-devices informations");
    osEnv()->addCliAppHelp("--robot-network-info", "shows robot network-interfaces informations");
    osEnv()->addCliAppHelp("--robot-uptime", "shows robot uptime interval");

    osEnv()->addCliAppHelp("--ls-robot-modules", "shows modules (internal satellites) from robot");

    osEnv()->addCliAppHelp("--sat-config <SAT_NAME>", "shows satellite configuration, if it exists");
    osEnv()->addCliAppHelp("--sat-trans <SAT_NAME>", "shows satellite command/control Transactions");
    osEnv()->addCliAppHelp("--sat-tick <SAT_NAME>", "shows satellite tick parameters, if db exists");

    osEnv()->addCliAppHelp("--ls-databases", "shows active pair-databases from robot");
    osEnv()->addCliAppHelp("--ls-db-keys <DB_NAME>", "shows pair-keys from a robot database");
    osEnv()->addCliAppHelp("--show-db-content <DB_NAME>", "prints JSON database content");
    osEnv()->addCliAppHelp("--show-var-content <DB_NAME.KEY_NAME>", "prints JSON variable content");
    osEnv()->addCliAppHelp("--human-readable", "prints contents as human-readable JSON structure");

    osEnv()->addCliAppHelp("--ls-stream-channels", "shows streaming channels from robot");
    osEnv()->addCliAppHelp("--ls-service-channels", "shows service channels from robot");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

